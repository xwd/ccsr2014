#ifndef SHIFTREDUCEHYPOTHESIS_H_
#define SHIFTREDUCEHYPOTHESIS_H_


#include "parser/ShiftReduceLattice.h"
#include "parser/supercat.h"
//#include "parser/_parser.h"


using namespace std;

#define MAX_UNARY_ACTIONS 3
#define SHIFT 0
#define UNARY 1
#define COMBINE 2
#define FINISH 3


namespace NLP
{
namespace CCG
{

class ShiftReduceHypothesis
{
public:
    ShiftReduceHypothesis(SuperCat *stackTopCat,
	    ShiftReduceHypothesis *prevHypo,
	    ShiftReduceHypothesis *prevStack,
	    size_t nextInputIndex,
	    size_t unaryActionCount,
	    size_t stackSize,
	    double totalScore,
	    bool finished,
	    size_t stackTopAction,
	    size_t totalActionCount)
    : m_stackTopSuperCat(stackTopCat),
      m_prevHypo(prevHypo),
      m_prevStack(prevStack),
      m_nextInputIndex(nextInputIndex),
      m_unaryActionCount(unaryActionCount),
      m_stackSize(stackSize),
      m_totalScore(totalScore),
      m_finished(finished),
      m_stackTopAction(stackTopAction),
      m_totalActionCount(totalActionCount){}

    ~ShiftReduceHypothesis();


    /*
    void *operator new(size_t size, Pool *pool) {
	return (void*)pool->alloc(size);
    }
    */

    //void operator delete(void *, Pool *pool) { /* do nothing */ }

    void Shift(size_t index, SuperCat *superCat, ShiftReduceLattice *lattice, Pool *pool,
	    size_t unaryActionCount, double totalScore);
    void Unary(size_t index, SuperCat *superCat, ShiftReduceLattice *lattice, Pool *pool,
	    size_t unaryActionCount, double totalScore);
    void Combine(size_t index, SuperCat *superCat, ShiftReduceLattice *lattice, Pool *pool,
	    size_t unaryActionCount, double totalScore);
    void Finish(size_t index, ShiftReduceLattice *lattice);

    ShiftReduceHypothesis *GetPrevHypo()
    {
	return m_prevHypo;
    }

    ShiftReduceHypothesis *GetPrvStack()
    {
	return m_prevStack;
    }

    size_t GetNextInputIndex()
    {
	return m_nextInputIndex;
    }

    SuperCat *GetStackTopSuperCat()
    {
	return m_stackTopSuperCat;
    }

    void ReSetUnaryActionCount()
    {
	m_unaryActionCount = 0;
    }

    size_t GetUnaryActionCount()
    {
	return m_unaryActionCount;
    }

    size_t GetStackSize()
    {
	return m_stackSize;
    }

    double GetTotalScore()
    {
	return m_totalScore;
    }


    void SetTotalScore(double score)
    {
	m_totalScore = score;
    }

    bool IsFinished(size_t NWORDS, bool allowFragTree)
    {
	if (allowFragTree)
	    return m_nextInputIndex == NWORDS;
	else
	    return m_nextInputIndex == NWORDS && m_stackSize == 1 ;
    }

    size_t GetStackTopAction()
    {
	return m_stackTopAction;
    }

    size_t GetTotalActionCount()
    {
	return m_totalActionCount;
    }


private:
    SuperCat *m_stackTopSuperCat;
    ShiftReduceHypothesis *m_prevHypo;
    ShiftReduceHypothesis *m_prevStack;
    size_t m_nextInputIndex;
    size_t m_unaryActionCount;
    size_t m_stackSize;
    double m_totalScore;
    bool m_finished;
    size_t m_stackTopAction;
    size_t m_totalActionCount;

};


class ShiftReduceAction
{
public:
    ShiftReduceAction(const size_t action, SuperCat *supercat)
    : m_action(action),
      m_supercat(supercat),
      m_cat(0){}

    ShiftReduceAction(const size_t action, const Cat *cat)
    : m_action(action),
      m_supercat(0),
      m_cat(cat){}

    size_t GetAction()
    {
	return m_action;
    }

    SuperCat *GetSuperCat()
    {
	return m_supercat;
    }

    const Cat *GetCat()
    {
	return m_supercat->GetCat();
    }

    friend bool operator==(ShiftReduceAction &actionA, ShiftReduceAction &actionB);

private:
    size_t m_action;
    SuperCat *m_supercat;
    const Cat *m_cat;
};

inline bool operator==(ShiftReduceAction &actionA, ShiftReduceAction &actionB)
{
   return actionA.m_action == actionB.m_action && actionA.GetCat() == actionB.GetCat();
}

}
}


#endif /* SHIFTREDUCEHYPOTHESIS_H_ */
