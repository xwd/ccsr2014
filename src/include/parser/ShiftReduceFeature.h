#ifndef SHIFTREDUCEFEATURE_H_
#define SHIFTREDUCEFEATURE_H_

#include "parser/ShiftReduceAttributes.h"
#include "parser/ShiftReduceHypothesis.h"
#include "tree/_baseimpl.h"

#include <string>



namespace NLP
{
namespace CCG
{

class ShiftReduceContext;


using namespace NLP::HashTable;
using namespace NLP::Tree;

// 1
const size_t s0wp = 1;
const size_t s1wp = 2;

const size_t s0c_ = 3;
const size_t s1c_ = 4;

const size_t s0pc = 5;
const size_t s1pc = 6;
const size_t s2pc = 7;
const size_t s3pc = 8;

const size_t s0wc = 9;
const size_t s1wc = 10;
const size_t s2wc = 11;
const size_t s3wc = 12;


// 2
const size_t q0wp = 13;
const size_t q1wp = 14;
const size_t q2wp = 15;
const size_t q3wp = 16;


/*
const static size_t s0lpc = 1;
const static size_t s0lwc = 2;
const static size_t s0rpc = 3;
const static size_t s0rwc = 4;
const static size_t s0upc = 5;
const static size_t s0uwc = 6;
const static size_t s1lpc = 7;
const static size_t s1lwc = 8;
const static size_t s1rpc = 9;
const static size_t s1rwc = 10;
const static size_t s1upc = 11;
const static size_t s1uwc = 12;


// 4
//const static size_t s0wcs1wc = 1;
//const static size_t s0cs1w = 2;
//const static size_t s0ws1c = 3;
//const static size_t s0cs1c = 4;
const static size_t s0wcq0wp = 5;
const static size_t s0cq0wp = 6;

const static size_t s0wcq0p = 7;
const static size_t s0cq0p = 8;

const static size_t s1wcq0wp = 9;
const static size_t s1cq0wp = 10;

const static size_t s1wcq0p = 11;
const static size_t s1cq0p = 12;


//const static size_t s0wcs1cq0p = 1;
//const static size_t s0cs1wcq0p = 2;
//const static size_t s0cs1cq0wp = 3;
//const static size_t s0cs1cq0p = 4;
const static size_t s0ps1pq0p = 5;
const static size_t s0pq0pq1p = 10;
const static size_t s0ps1ps2p = 15;
//const static size_t s0wcq0pq1p = 6;
//const static size_t s0cq0wpq1p = 7;
//const static size_t s0cq0pq1wp = 8;
//const static size_t s0cq0pq1p = 9;
//const static size_t s0wcs1cs2c = 11;
//const static size_t s0cs1wcs2c = 12;
//const static size_t s0cs1cs2wc = 13;
//const static size_t s0cs1cs2c = 14;


// 6
const static size_t s0cs0lcS0rc = 1;
const static size_t s1cs1lcs1rc = 2;

//const static size_t s0cs0rcq0p = 3;

const static size_t s0cs0rcq0w = 4;
const static size_t s0cs0lcs1w = 5;

const static size_t s0cs0lcs1c = 6;
const static size_t s0cs1cs1rc = 7;

//const static size_t s0ws1cs1rc = 8;

*/

class ShiftReduceFeature
{
private:
    Categories &cats;
    const Lexicon lexicon;

    SwpAttributes swpAttrs;
    ScAttributes scAttrs;
    SpcAttributes spcAttrs;
    SwcAttributes swcAttrs;

    QwpAttributes qwpAttrs;

/*

    SlurpcAttributes slurpcAttrs;
    SlurwcAttributes slurwcAttrs;

    S0wcS1wcAttributes s0wcs1wcAttrs;
    S0cS1wAttributes s0cs1wAttrs;
    S0wS1cAttributes s0ws1cAttrs;
    S0cS1cAttributes s0cs1cAttrs;
    SwcQwpAttributes swcqwpAttrs;
    ScQwpAttributes scqwpAttrs;
    SwcQpAttributes swcqpAttrs;
    ScQpAttributes scqpAttrs;

    S0wcS1cQ0pAttributes s0wcs1cq0pAttrs;
    S0cS1wcQ0pAttributes s0cs1wcq0pAttrs;
    S0cS1cQ0wpAttributes s0cs1cq0wpAttrs;
    S0cS1cQ0pAttributes s0cs1cq0pAttrs;
    S0pSQpQSpAttributes s0psqpqspAttrs;
    S0wcQ0pQ1pAttributes s0wcq0pq1pAttrs;
    S0cQ0wpQ1pAttributes s0cq0wpq1pAttrs;
    S0cQ0pQ1wpAttributes s0cq0pq1wpAttrs;
    S0cQ0pQ1pAttributes s0cq0pq1pAttrs;
    S0wcS1cS2cAttributes s0wcs1cs2cAttrs;
    S0cS1wcS2cAttributes s0cs1wcs2cAttrs;
    S0cS1cS2wcAttributes s0cs1cs2wcAttrs;
    S0cS1cS2cAttributes s0cs1cs2cAttrs;


    S0cScScAttributes s0cScScAttrs;
    S0cS0RcQ0pAttributes s0cS0RcQ0pAttrs;
    S0cScQ0S1wAttributes s0cScQ0S1wAttrs;
    S0wS1cS1RcAttributes s0wS1cS1RcAttrs;

*/


public:
    ShiftReduceFeature(Categories &cats, const Lexicon &lexicon):
	cats(cats), lexicon(lexicon){}
    virtual ~ShiftReduceFeature(void){ /* do nothing */ }

    double GetOrUpdateWeight(std::vector<size_t> &ids, bool getOrUpdate, bool correct, ShiftReduceContext &context, ShiftReduceAction &action,
	    const Words &words, const Words &tags);

    void SetWeights(std::istream &in, size_t type);
    bool SaveWeights(std::ofstream &out);

    const Cat *GetCat(std::string tmp);


};


struct SwpValue
{
    friend std::ostream &operator<<(std::ostream&, const SwpValue&);

    const size_t type;
    const Word wordValue;
    const Word tagValue;
    ShiftReduceAction &action;

    SwpValue(const size_t type, const Word wordValue, const Word tagValue, ShiftReduceAction &action)
    : type(type), wordValue(wordValue), tagValue(tagValue), action(action) {}


    Hash hash(void) const
    {
	Hash h(static_cast<ulong>(type));
	h += static_cast<ulong>(wordValue);
	h += static_cast<ulong>(tagValue);
	h += action.GetSuperCat()->cat->rhash;
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const SwpValue &other) const
    {
	if(other.type == type && other.wordValue == wordValue && other.tagValue == tagValue && other.action == action)
	    return true;
	else
	    return false;
    }
};

inline std::ostream &operator<<(std::ostream& out, const SwpValue& value)
{
    out << value.type << " " << value.wordValue.str() << " " << value.tagValue.str() << " " << value.action.GetAction() << " " << value.action.GetCat();
    return out;
}


struct ScValue
{
    friend std::ostream &operator<<(std::ostream&, const ScValue&);

    const size_t type;
    const Cat *cat;
    ShiftReduceAction &action;

    ScValue(const size_t type, const Cat *cat, ShiftReduceAction &action)
    : type(type), cat(cat), action(action) {}


    Hash hash(void) const
    {
	Hash h(static_cast<ulong>(type));
	h += cat->rhash;
	h += action.GetSuperCat()->cat->rhash;
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const ScValue &other) const
    {
	if(other.type == type && other.action == action)
	{
	    if (other.cat == cat)
		return true;
	    else return false;
	}
	else
	{
	    return false;
	}
    }

};

inline std::ostream &operator<<(std::ostream& out, const ScValue& value)
{
    out << value.type << " " << value.cat << " " << value.action.GetAction() << " " << value.action.GetCat();
    return out;
}

struct SpcValue
{
    friend std::ostream &operator<<(std::ostream&, const SpcValue&);

    const size_t type;
    const Word tagValue;
    const Cat *cat;
    ShiftReduceAction &action;

    SpcValue(const size_t type, const Word tagValue, const Cat *cat, ShiftReduceAction &action)
    : type(type), tagValue(tagValue), cat(cat), action(action) {}


    Hash hash(void) const
    {
	Hash h(static_cast<ulong>(type));
	h += static_cast<ulong>(tagValue);
	h += cat->rhash;
	h += action.GetSuperCat()->cat->rhash;
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const SpcValue &other) const
    {
	if(other.type == type && other.tagValue == tagValue && other.action == action)
	{
	    if (other.cat == cat)
		return true;
	    else return false;
	}
	else
	{
	    return false;
	}
    }

};

inline std::ostream &operator<<(std::ostream& out, const SpcValue& value) {
	out << value.type << " " << value.tagValue.str() << " " << value.cat << " " << value.action.GetAction() << " " << value.action.GetCat();
	return out;
}


struct SwcValue
{
    friend std::ostream &operator<<(std::ostream&, const SwcValue&);

    const size_t type;
    const Word wordValue;
    const Cat *cat;
    ShiftReduceAction &action;

    SwcValue(const size_t type, const Word wordValue, const Cat *cat, ShiftReduceAction &action)
    : type(type), wordValue(wordValue), cat(cat), action(action) {}


    Hash hash(void) const
    {
	Hash h(static_cast<ulong>(type));
	h += static_cast<ulong>(wordValue);
	h += cat->rhash;
	h += action.GetSuperCat()->cat->rhash;
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const SwcValue &other) const
    {
	if(other.type == type && other.wordValue == wordValue && other.action == action)
	{
	    if (other.cat == cat)
		return true;
	    else return false;
	}
	else
	{
	    return false;
	}
    }

};

inline std::ostream &operator<<(std::ostream& out, const SwcValue& value) {
	out << value.type << " " << value.wordValue.str() << " " << value.cat << " " << value.action.GetAction() << " " << value.action.GetCat();
	return out;
}

// 2

struct QwpValue
{
    friend std::ostream &operator<<(std::ostream&, const QwpValue&);

    const size_t type;
    const Word wordValue;
    const Word tagValue;
    ShiftReduceAction &action;

    QwpValue(const size_t type, const Word wordValue, const Word tagValue, ShiftReduceAction &action)
    : type(type), wordValue(wordValue), tagValue(tagValue), action(action) {}


    Hash hash(void) const
    {
	Hash h(static_cast<ulong>(wordValue));
	h += static_cast<ulong>(type);
	h += static_cast<ulong>(tagValue);
	h += action.GetSuperCat()->cat->rhash;
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const QwpValue &other) const
    {
	if(other.type == type && other.wordValue == wordValue &&
		other.tagValue == tagValue && other.action == action)
	    return true;
	else
	    return false;
    }
};

inline std::ostream &operator<<(std::ostream& out, const QwpValue& value) {
	out << value.type << " " << value.wordValue.str() << " " << value.tagValue.str() << " " << value.action.GetAction() << " " << value.action.GetCat();
	return out;
}
/*

// 3

struct SlurpcValue
{
    const size_t type;
    const Word tagValue;
    const Cat *cat;
    ShiftReduceAction &action;

    SlurpcValue(const size_t type, const Word tagValue, const Cat *cat, ShiftReduceAction &action)
    : type(type), tagValue(tagValue), cat(cat), action(action) {}

    Hash hash(void) const
    {
	Hash h(static_cast<ulong>(tagValue));
	h += static_cast<ulong>(type);
	h += cat->rhash;
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const SlurpcValue &other) const
    {
	if(other.type == type && other.tagValue == tagValue && other.action == action)
	{
	    if (other.cat == cat)
		return true;
	    else
		return false;
	}
	else
	{
	    return false;
	}
    }
};



struct SlurwcValue
{
    const size_t type;
    const Word wordValue;
    const Cat *cat;
    ShiftReduceAction &action;

    SlurwcValue(const size_t type, const Word wordValue, const Cat *cat, ShiftReduceAction &action)
    : type(type), wordValue(wordValue), cat(cat), action(action) {}

    Hash hash(void) const
    {
	Hash h(static_cast<ulong>(wordValue));
	h += static_cast<ulong>(type);
	h += cat->rhash;
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const SlurwcValue &other) const
    {
	if(other.type == type && other.wordValue == wordValue && other.action == action)
	{
	    if (other.cat == cat)
		return true;
	    else
		return false;
	}
	else
	{
	    return false;
	}
    }
};


// 4

struct S0wcS1wcValue
{
    const Word wordValueS0;
    const Cat *catS0;
    const Word wordValueS1;
    const Cat *catS1;
    ShiftReduceAction &action;

    S0wcS1wcValue(const Word wordValueS0, const Cat *catS0, const Word wordValueS1, const Cat *catS1, ShiftReduceAction &action)
    : wordValueS0(wordValueS0), catS0(catS0), wordValueS1(wordValueS1), catS1(catS1), action(action) {}

    Hash hash(void) const
    {
	Hash h(static_cast<ulong>(wordValueS0));
	h += catS0->rhash;
	h += static_cast<ulong>(wordValueS1);
	h += catS1->rhash;
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const S0wcS1wcValue &other) const
    {
	if(other.wordValueS0 == wordValueS0 && other.wordValueS1 == wordValueS1 &&
		other.action == action)
	{
	    if (other.catS0 == catS0 && other.catS1 == catS1)
		return true;
	    else
		return false;
	}
	else
	{
	    return false;
	}
    }
};



struct S0cS1wValue
{
    const Cat *catS0;
    const Word wordValueS1;
    ShiftReduceAction &action;

    S0cS1wValue(const Cat *catS0, const Word wordValueS1, ShiftReduceAction &action)
    : catS0(catS0), wordValueS1(wordValueS1), action(action) {}

    Hash hash(void) const
    {
	Hash h(catS0->rhash);
	h += static_cast<ulong>(wordValueS1);
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const S0cS1wValue &other) const
    {
	if(other.wordValueS1 == wordValueS1 &&
		other.action == action)
	{
	    if (other.catS0 == catS0)
		return true;
	    else
		return false;
	}
	else
	{
	    return false;
	}
    }
};


struct S0wS1cValue
{
    const Word wordValueS0;
    const Cat *catS1;
    ShiftReduceAction &action;

    S0wS1cValue(const Word wordValueS0, const Cat *catS1, ShiftReduceAction &action)
    : wordValueS0(wordValueS0), catS1(catS1),  action(action) {}

    Hash hash(void) const
    {
	Hash h(static_cast<ulong>(wordValueS0));
	h += catS1->rhash;
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const S0wS1cValue &other) const
    {
	if(other.wordValueS0 == wordValueS0 &&
		other.action == action)
	{
	    if (other.catS1 == catS1)
		return true;
	    else
		return false;
	}
	else
	{
	    return false;
	}
    }
};


struct S0cS1cValue
{
    const Cat *catS0;
    const Cat *catS1;
    ShiftReduceAction &action;

    S0cS1cValue(const Cat *catS0, const Cat *catS1, ShiftReduceAction &action)
    : catS0(catS0), catS1(catS1),  action(action) {}

    Hash hash(void) const
    {
	Hash h(catS0->rhash);
	h += catS1->rhash;
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const S0cS1cValue &other) const
    {
	if(other.action == action)
	{
	    if (other.catS0 == catS0 && other.catS1 == catS1)
		return true;
	    else
		return false;
	}
	else
	{
	    return false;
	}
    }
};


struct SwcQwpValue
{
    const size_t type;
    const Word wordValueS;
    const Cat *catS;
    const Word wordValueQ;
    const Word tagValueQ;
    ShiftReduceAction &action;

    SwcQwpValue(const size_t type, const Word wordValueS, const Cat *catS, const Word wordValueQ, const Word tagValueQ, ShiftReduceAction &action)
    : type(type), wordValueS(wordValueS), catS(catS), wordValueQ(wordValueQ), tagValueQ(tagValueQ), action(action) {}

    Hash hash(void) const
    {
	Hash h(static_cast<ulong>(type));
	h += static_cast<ulong>(wordValueS);
	h += catS->rhash;
	h += static_cast<ulong>(wordValueQ);
	h += static_cast<ulong>(tagValueQ);
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const SwcQwpValue &other) const
    {
	if(other.type == type && other.wordValueS == wordValueS && other.wordValueQ == wordValueQ &&
		other.tagValueQ == tagValueQ && other.action == action)
	{
	    if (other.catS == catS)
		return true;
	    else
		return false;
	}
	else
	{
	    return false;
	}
    }
};


struct ScQwpValue
{
    const size_t type;
    const Cat *catS;
    const Word wordValueQ;
    const Word tagValueQ;
    ShiftReduceAction &action;

    ScQwpValue(const size_t type, const Cat *catS, const Word wordValueQ, const Word tagValueQ, ShiftReduceAction &action)
    : type(type), catS(catS), wordValueQ(wordValueQ), tagValueQ(tagValueQ), action(action) {}

    Hash hash(void) const
    {
	Hash h(static_cast<ulong>(type));
	h += catS->rhash;
	h += static_cast<ulong>(wordValueQ);
	h += static_cast<ulong>(tagValueQ);
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const ScQwpValue &other) const
    {
	if (other.type == type && other.wordValueQ == wordValueQ &&
		other.tagValueQ == tagValueQ && other.action == action)
	{
	    if (other.catS == catS)
		return true;
	    else
		return false;
	}
	else
	{
	    return false;
	}
    }
};


struct SwcQpValue
{
    const size_t type;
    const Word wordValueS;
    const Cat *catS;
    const Word tagValueQ;
    ShiftReduceAction &action;

    SwcQpValue(const size_t type, const Word wordValueS, const Cat *catS, const Word tagValueQ, ShiftReduceAction &action)
    : type(type), wordValueS(wordValueS), catS(catS), tagValueQ(tagValueQ), action(action) {}

    Hash hash(void) const
    {
	Hash h(static_cast<ulong>(type));
	h += static_cast<ulong>(wordValueS);
	h += catS->rhash;
	h += static_cast<ulong>(tagValueQ);
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const SwcQpValue &other) const
    {
	if(other.type == type && other.wordValueS == wordValueS && other.tagValueQ == tagValueQ &&
		other.action == action)
	{
	    if (other.catS == catS)
		return true;
	    else
		return false;
	}
	else
	{
	    return false;
	}
    }
};


struct ScQpValue
{
    const size_t type;
    const Cat *catS;
    const Word tagValueQ;
    ShiftReduceAction &action;

    ScQpValue(const size_t type, const Cat *catS, const Word tagValueQ, ShiftReduceAction &action)
    : type(type), catS(catS), tagValueQ(tagValueQ), action(action) {}

    Hash hash(void) const
    {
	Hash h(static_cast<ulong>(type));
	h += catS->rhash;
	h += static_cast<ulong>(tagValueQ);
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const ScQpValue &other) const
    {
	if(other.type == type && other.tagValueQ == tagValueQ && other.action == action)
	{
	    if (other.catS == catS)
		return true;
	    else
		return false;
	}
	else
	{
	    return false;
	}

    }
};


// 5

struct S0wcS1cQ0pValue
{
    const Word wordValueS0;
    const Cat *catS0;
    const Cat *catS1;
    const Word tagValueQ0;
    ShiftReduceAction &action;

    S0wcS1cQ0pValue(const Word wordValueS0, const Cat *catS0, const Cat *catS1, const Word tagValueQ0, ShiftReduceAction &action)
    : wordValueS0(wordValueS0), catS0(catS0), catS1(catS1), tagValueQ0(tagValueQ0), action(action) {}

    Hash hash(void) const
    {
	Hash h(static_cast<ulong>(wordValueS0));
	h += catS0->rhash;
	h += catS1->rhash;
	h += static_cast<ulong>(tagValueQ0);
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const S0wcS1cQ0pValue &other) const
    {
	if(other.wordValueS0 == wordValueS0 && other.tagValueQ0 == tagValueQ0 &&
		other.action == action)
	{
	    if (other.catS0 == catS0 && other.catS1 == catS1)
		return true;
	    else
		return false;
	}
	else
	{
	    return false;
	}
    }
};


struct S0cS1wcQ0pValue
{
    const Cat *catS0;
    const Word wordValueS1;
    const Cat *catS1;
    const Word tagValueQ0;
    ShiftReduceAction &action;

    S0cS1wcQ0pValue(const Cat *catS0, const Word wordValueS1, const Cat *catS1, const Word tagValueQ0, ShiftReduceAction &action)
    : catS0(catS0), wordValueS1(wordValueS1), catS1(catS1), tagValueQ0(tagValueQ0), action(action) {}

    Hash hash(void) const
    {
	Hash h(catS0->rhash);
	h += static_cast<ulong>(wordValueS1);
	h += catS1->rhash;
	h += static_cast<ulong>(tagValueQ0);
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const S0cS1wcQ0pValue &other) const
    {
	if(other.wordValueS1 == wordValueS1 && other.tagValueQ0 == tagValueQ0 &&
		other.action == action)
	{
	    if (other.catS0 == catS0 && other.catS1 == catS1)
		return true;
	    else
		return false;
	}
	else
	{
	    return false;
	}
    }
};


struct S0cS1cQ0wpValue
{
    const Cat *catS0;
    const Cat *catS1;
    const Word wordValueQ0;
    const Word tagValueQ0;
    ShiftReduceAction &action;

    S0cS1cQ0wpValue(const Cat *catS0, const Cat *catS1, const Word wordValueQ0, const Word tagValueQ0, ShiftReduceAction &action)
    : catS0(catS0), catS1(catS1), wordValueQ0(wordValueQ0), tagValueQ0(tagValueQ0), action(action) {}

    Hash hash(void) const
    {
	Hash h(catS0->rhash);
	h += catS1->rhash;
	h += static_cast<ulong>(wordValueQ0);
	h += static_cast<ulong>(tagValueQ0);
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const S0cS1cQ0wpValue &other) const
    {
	if(other.wordValueQ0 == wordValueQ0 && other.tagValueQ0 == tagValueQ0 &&
		other.action == action)
	{
	    if (other.catS0 == catS0 && other.catS1 == catS1)
		return true;
	    else
		return false;
	}
	else
	{
	    return false;
	}
    }
};


struct S0cS1cQ0pValue
{
    const Cat *catS0;
    const Cat *catS1;
    const Word tagValueQ0;
    ShiftReduceAction &action;

    S0cS1cQ0pValue(const Cat *catS0, const Cat *catS1, const Word tagValueQ0, ShiftReduceAction &action)
    : catS0(catS0), catS1(catS1), tagValueQ0(tagValueQ0), action(action) {}

    Hash hash(void) const
    {
	Hash h(catS0->rhash);
	h += catS1->rhash;
	h += static_cast<ulong>(tagValueQ0);
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const S0cS1cQ0pValue &other) const
    {
	if(other.tagValueQ0 == tagValueQ0 &&
		other.action == action)
	{
	    if (other.catS0 == catS0 && other.catS1 == catS1)
		return true;
	    else
		return false;
	}
	else
	{
	    return false;
	}
    }
};


struct S0pSQpQSpValue
{
    const size_t type;
    const Word tagValueS0;
    const Word tagValueSQ;
    const Word tagValueQS;
    ShiftReduceAction &action;

    S0pSQpQSpValue(const size_t type, const size_t type, const Word tagValueS0, const Word tagValueSQ, const Word tagValueQS, ShiftReduceAction &action)
    : type(type), tagValueS0(tagValueS0), tagValueSQ(tagValueSQ), tagValueQS(tagValueQS), action(action) {}

    Hash hash(void) const
    {
	Hash h(static_cast<ulong>(tagValueS0));
	h += static_cast<ulong>(type);
	h += static_cast<ulong>(tagValueSQ);
	h += static_cast<ulong>(tagValueQS);
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const S0pSQpQSpValue &other) const
    {
	if(other.type == type && other.tagValueS0 == tagValueS0 &&
		other.tagValueSQ == tagValueSQ && other.tagValueQS == tagValueQS &&
		other.action == action)
	    return true;
	else
	    return false;

    }
};


struct S0wcQ0pQ1pValue
{
    const Word wordValueS0;
    const Cat *catS0;
    const Word tagValueQ0;
    const Word tagValueQ1;
    ShiftReduceAction &action;

    S0wcQ0pQ1pValue(const Word wordValueS0, const Cat *catS0, const Word tagValueQ0, const Word tagValueQ1, ShiftReduceAction &action)
    : wordValueS0(wordValueS0), catS0(catS0), tagValueQ0(tagValueQ0), tagValueQ1(tagValueQ1), action(action) {}

    Hash hash(void) const
    {
	Hash h(static_cast<ulong>(wordValueS0));
	h += catS0->rhash;
	h += static_cast<ulong>(tagValueQ0);
	h += static_cast<ulong>(tagValueQ1);
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const S0wcQ0pQ1pValue &other) const
    {
	if(other.wordValueS0 == wordValueS0 && other.tagValueQ0 == tagValueQ0 && other.tagValueQ1 == tagValueQ1 &&
		other.action == action)
	{
	    if (other.catS0 == catS0)
		return true;
	    else
		return false;
	}
	else
	{
	    return false;
	}
    }
};


struct S0cQ0wpQ1pValue
{
    const Cat *catS0;
    const Word wordValueQ0;
    const Word tagValueQ0;
    const Word tagValueQ1;
    ShiftReduceAction &action;

    S0cQ0wpQ1pValue(const Cat *catS0, const Word wordValueQ0, const Word tagValueQ0, const Word tagValueQ1, ShiftReduceAction &action)
    : catS0(catS0), wordValueQ0(wordValueQ0), tagValueQ0(tagValueQ0), tagValueQ1(tagValueQ1), action(action) {}

    Hash hash(void) const
    {
	Hash h(catS0->rhash);
	h += static_cast<ulong>(wordValueQ0);
	h += static_cast<ulong>(tagValueQ0);
	h += static_cast<ulong>(tagValueQ1);
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const S0cQ0wpQ1pValue &other) const
    {
	if(other.wordValueQ0 == wordValueQ0 && other.tagValueQ0 == tagValueQ0 && other.tagValueQ1 == tagValueQ1 &&
		other.action == action)
	{
	    if (other.catS0 == catS0)
		return true;
	    else
		return false;
	}
	else
	{
	    return false;
	}
    }
};


struct S0cQ0pQ1wpValue
{
    const Cat *catS0;
    const Word tagValueQ0;
    const Word wordValueQ1;
    const Word tagValueQ1;
    ShiftReduceAction &action;

    S0cQ0pQ1wpValue(const Cat *catS0, const Word tagValueQ0, const Word wordValueQ1, const Word tagValueQ1, ShiftReduceAction &action)
    : catS0(catS0), tagValueQ0(tagValueQ0), wordValueQ1(wordValueQ1), tagValueQ1(tagValueQ1), action(action) {}

    Hash hash(void) const
    {
	Hash h(catS0->rhash);
	h += static_cast<ulong>(tagValueQ0);
	h += static_cast<ulong>(wordValueQ1);
	h += static_cast<ulong>(tagValueQ1);
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const S0cQ0pQ1wpValue &other) const
    {
	if(other.tagValueQ0 == tagValueQ0 && other.wordValueQ1 == wordValueQ1 && other.tagValueQ1 == tagValueQ1 &&
		other.action == action)
	{
	    if (other.catS0 == catS0)
		return true;
	    else
		return false;
	}
	else
	{
	    return false;
	}
    }
};


struct S0cQ0pQ1pValue
{
    const Cat *catS0;
    const Word tagValueQ0;
    const Word tagValueQ1;
    ShiftReduceAction &action;

    S0cQ0pQ1pValue(const Cat *catS0, const Word tagValueQ0, const Word tagValueQ1, ShiftReduceAction &action)
    : catS0(catS0), tagValueQ0(tagValueQ0), tagValueQ1(tagValueQ1), action(action) {}

    Hash hash(void) const
    {
	Hash h(catS0->rhash);
	h += static_cast<ulong>(tagValueQ0);
	h += static_cast<ulong>(tagValueQ1);
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const S0cQ0pQ1pValue &other) const
    {
	if(other.tagValueQ0 == tagValueQ0 && other.tagValueQ1 == tagValueQ1 &&
		other.action == action)
	{
	    if (other.catS0 == catS0)
		return true;
	    else
		return false;
	}
	else
	{
	    return false;
	}
    }
};

struct S0wcS1cS2cValue
{
    const Word wordValueS0;
    const Cat *catS0;
    const Cat *catS1;
    const Cat *catS2;
    ShiftReduceAction &action;

    S0wcS1cS2cValue(const Word wordValueS0, const Cat *catS0, const Cat *catS1, const Cat *catS2, ShiftReduceAction &action)
    : wordValueS0(wordValueS0), catS0(catS0), catS1(catS1), catS2(catS2), action(action) {}

    Hash hash(void) const
    {
	Hash h(static_cast<ulong>(wordValueS0));
	h += catS0->rhash;
	h += catS1->rhash;
	h += catS2->rhash;
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const S0wcS1cS2cValue &other) const
    {
	if(other.wordValueS0 == wordValueS0 &&
		other.action == action)
	{
	    if (other.catS0 == catS0 && other.catS1 == catS1 && other.catS2 == catS2)
		return true;
	    else
		return false;
	}
	else
	{
	    return false;
	}
    }
};


struct S0cS1wcS2cValue
{
    const Cat *catS0;
    const Word wordValueS1;
    const Cat *catS1;
    const Cat *catS2;
    ShiftReduceAction &action;

    S0cS1wcS2cValue(const Cat *catS0, const Word wordValueS1, const Cat *catS1, const Cat *catS2, ShiftReduceAction &action)
    : catS0(catS0), wordValueS1(wordValueS1), catS1(catS1), catS2(catS2), action(action) {}

    Hash hash(void) const
    {
	Hash h(catS0->rhash);
	h += static_cast<ulong>(wordValueS1);
	h += catS1->rhash;
	h += catS2->rhash;
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const S0cS1wcS2cValue &other) const
    {
	if(other.wordValueS1 == wordValueS1 &&
		other.action == action)
	{
	    if (other.catS0 == catS0 && other.catS1 == catS1 && other.catS2 == catS2)
		return true;
	    else
		return false;
	}
	else
	{
	    return false;
	}
    }
};


struct S0cS1cS2wcValue
{
    const Cat *catS0;
    const Cat *catS1;
    const Word wordValueS2;
    const Cat *catS2;
    ShiftReduceAction &action;

    S0cS1cS2wcValue(const Cat *catS0, const Cat *catS1, const Word wordValueS2, const Cat *catS2, ShiftReduceAction &action)
    : catS0(catS0), catS1(catS1), wordValueS2(wordValueS2), catS2(catS2), action(action) {}

    Hash hash(void) const
    {
	Hash h(catS0->rhash);
	h += catS1->rhash;
	h += static_cast<ulong>(wordValueS2);
	h += catS2->rhash;
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const S0cS1cS2wcValue &other) const
    {
	if(other.wordValueS2 == wordValueS2 &&
		other.action == action)
	{
	    if (other.catS0 == catS0 && other.catS1 == catS1 && other.catS2 == catS2)
		return true;
	    else
		return false;
	}
	else
	{
	    return false;
	}
    }
};


struct S0cS1cS2cValue
{
    const Cat *catS0;
    const Cat *catS1;
    const Cat *catS2;
    ShiftReduceAction &action;

    S0cS1cS2cValue(const Cat *catS0, const Cat *catS1, const Cat *catS2, ShiftReduceAction &action)
    : catS0(catS0), catS1(catS1), catS2(catS2), action(action) {}

    Hash hash(void) const
    {
	Hash h(catS0->rhash);
	h += catS1->rhash;
	h += catS2->rhash;
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const S0cS1cS2cValue &other) const
    {
	if(other.action == action)
	{
	    if (other.catS0 == catS0 && other.catS1 == catS1 && other.catS2 == catS2)
		return true;
	    else
		return false;
	}
	else
	{
	    return false;
	}
    }
};


struct S0pS1pS2pValue
{
    const Word tagValueS0;
    const Word tagValueS1;
    const Word tagValueS2;
    ShiftReduceAction &action;

    S0pS1pS2pValue(const Word tagValueS0, const Word tagValueS1, const Word tagValueS2, ShiftReduceAction &action)
    : tagValueS0(tagValueS0), tagValueS1(tagValueS1), tagValueS2(tagValueS2), action(action) {}

    Hash hash(void) const
    {
	Hash h(static_cast<ulong>(tagValueS0));
	h += static_cast<ulong>(tagValueS1);
	h += static_cast<ulong>(tagValueS2);
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const S0pS1pS2pValue &other) const
    {
	if(other.action == action && other.tagValueS0 == tagValueS0 && other.tagValueS1 == tagValueS1
		&& other.tagValueS2 == tagValueS2)
	    return true;
	else
	    return false;
    }
};




// 6

struct S01cS01LcS01rcValue
{
    const size_t type;
    const Cat *cat0;
    const Cat *cat1;
    const Cat *cat2;
    ShiftReduceAction &action;

    S01cS01LcS01rcValue(const size_t type, const Cat *cat0, const Cat *cat1, const Cat *cat2, ShiftReduceAction &action)
    : type(type), cat0(cat0), cat1(cat1), cat2(cat2), action(action) {}

    Hash hash(void) const
    {
	Hash h(cat0->rhash);
	h += static_cast<ulong>(type);
	h += cat1->rhash;
	h += cat2->rhash;
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const S01cS01LcS01rcValue &other) const
    {
	if (other.type == type && other.action == action)
	    if (other.cat0 == cat0 && other.cat1 == cat1 && other.cat2 == cat2)
		return true;
	    else
		return false;
	else
	    return false;
    }
};


struct S0cS0RcQ0pValue
{
    const Cat *s0c;
    const Cat *s0rc;
    const Wrod q0p;
    ShiftReduceAction &action;

    S0cS0RcQ0pValue(const Cat *s0c, const Cat *s0rc, Word q0p, ShiftReduceAction &action)
    : s0c(s0c), s0rc(s0rc), q0p(q0p), action(action) {}

    Hash hash(void) const
    {
	Hash h(static_cast<ulong>(q0p));
	h += s0c->rhash;
	h += s0rc->rhash;
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const S0cS0RcQ0pValue &other) const
    {
	if(other.action == action && other.q0p == q0p)
	    if (other.s0c == s0c && other.s0rc == s0rc)
		return true;
	    else
		return false;
	else
	    return false;
    }
};


struct S0cS0LRcQ0S1wValue
{
    const size_t type;
    const Cat *s0c;
    const Cat *s0lrc;
    const Word word;
    ShiftReduceAction &action;

    S0cS0LRcQ0S1wValue(const size_t type, const Cat *s0c, const Cat *s0lrc, Word word, ShiftReduceAction &action)
    : type(type), s0c(s0c), s0lrc(s0lrc), word(word), action(action) {}

    Hash hash(void) const
    {
	Hash h(static_cast<ulong>(word));
	h += static_cast<ulong>(type);
	h += s0c->rhash;
	h += s0lrc->rhash;
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const S0cS0LRcQ0S1wValue &other) const
    {
	if(other.type == type && other.action == action && other.word == word)
	    if (other.s0c == s0c && other.s0lrc == s0lrc)
		return true;
	    else
		return false;
	else
	    return false;
    }
};



struct S0cS0LS1cS1S1RcValue
{
    const size_t type;
    const Cat *s0c;
    const Cat *s0lrc;
    const Cat *s1s1rc;
    ShiftReduceAction &action;

    S0cS0LS1cS1S1RcValue(const size_t type, const Cat *s0c, const Cat *s0lrc, Cat *s1s1rc, ShiftReduceAction &action)
    : type(type), s0c(s0c), s0lrc(s0lrc), s1s1rc(s1s1rc), action(action) {}

    Hash hash(void) const
    {
	Hash h(static_cast<ulong>(type));
	h += s0c->rhash;
	h += s0lrc->rhash;
	h += s1s1rc->rhash;
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const S0cS0LS1cS1S1RcValue &other) const
    {
	if(other.type == type && other.action == action)
	    if (other.s0c == s0c && other.s0lrc == s0lrc && other.s1s1rc == s1s1rc)
		return true;
	    else
		return false;
	else
	    return false;
    }
};



struct S0wS1cS1RcValue
{
    const Word s0w;
    const Cat *s1c;
    const Cat *s1rc;
    ShiftReduceAction &action;

    S0wS1cS1RcValue(const Word s0w, const Cat *s1c, const Cat *s1rc, ShiftReduceAction &action)
    : s0w(s0w), s1c(s1c), s1rc(s1rc), action(action) {}

    Hash hash(void) const
    {
	Hash h(static_cast<ulong>(s0w));
	h += s1c->rhash;
	h += s1rc->rhash;
	h += static_cast<ulong>(action.GetSuperCat()->cat);
	h += static_cast<ulong>(action.GetAction());
	return h;
    }

    bool equal(const S0wS1cS1RcValue &other) const
    {
	if(other.action == action && other.s0w == s0w)
	    if (other.s1c == s1c && other.s1rc == s1rc)
		return true;
	    else
		return false;
	else
	    return false;
    }
};
*/

}
}



#endif /* SHIFTREDUCEFEATURE_H_ */
