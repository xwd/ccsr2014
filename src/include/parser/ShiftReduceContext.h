#ifndef SHIFTREDUCECONTEXT_H_
#define SHIFTREDUCECONTEXT_H_

#include "parser/ShiftReduceHypothesis.h"

namespace NLP { namespace CCG {


class ShiftReduceHypothesis;

class ShiftReduceContext
{

public:
    const SuperCat *s0c, *s1c, *s2c, *s3c;
    const Word *q0w, *q1w, *q2w, *q3w;

    //RawTag s0tRaw, s1tRaw, s2tRaw, s3tRaw;
    //Tag s0p, s1p, s2p, s3p;
    const Word *q0p, *q1p, *q2p, *q3p;

    //RawWords s0wRaw, s1wRaw, s2wRaw, s3wRaw;
    std::vector<Word> s0w;
    std::vector<Word> s1w;
    std::vector<Word> s2w;
    std::vector<Word> s3w;

    std::vector<Word> s0p;
    std::vector<Word> s1p;
    std::vector<Word> s2p;
    std::vector<Word> s3p;


    Cat *s0lc, *s1lc, *s0uc, *s1uc, *s0rc, *s1rc;

    std::vector<Word> s0lw;
    std::vector<Word> s0uw;
    std::vector<Word> s1lw;
    std::vector<Word> s1uw;
    std::vector<Word> s0rw;
    std::vector<Word> s1rw;

    std::vector<Word> s0lp;
    std::vector<Word> s1lp;
    std::vector<Word> s0up;
    std::vector<Word> s1up;
    std::vector<Word> s0rp;
    std::vector<Word> s1rp;



    void LoadContext(ShiftReduceHypothesis *hypo, const Words &words, const Words &tags)
    {

	if (hypo->GetStackSize() == 0) return; // empty stack, shift only


	s0c = hypo->GetStackSize() >= 1 ? hypo->GetStackTopSuperCat() : 0;
	s1c = hypo->GetStackSize() >= 2 ? hypo->GetPrvStack()->GetStackTopSuperCat() : 0;
	s2c = hypo->GetStackSize() >= 3 ? hypo->GetPrvStack()->GetPrvStack()->GetStackTopSuperCat() : 0;
	s3c = hypo->GetStackSize() >= 4 ? hypo->GetPrvStack()->GetPrvStack()->GetPrvStack()->GetStackTopSuperCat() : 0;


	q0w = hypo->GetNextInputIndex() < words.size() ? &words[hypo->GetNextInputIndex()] : 0;
	q1w = hypo->GetNextInputIndex() + 1 < words.size() ? &words[hypo->GetNextInputIndex() + 1] : 0;
	q2w = hypo->GetNextInputIndex() + 2 < words.size() ? &words[hypo->GetNextInputIndex() + 2] : 0;
	q3w = hypo->GetNextInputIndex() + 3 < words.size() ? &words[hypo->GetNextInputIndex() + 3] : 0;


	q0p = hypo->GetNextInputIndex() < words.size() ? &tags[hypo->GetNextInputIndex()] : 0;
	q1p = hypo->GetNextInputIndex() + 1 < words.size() ? &tags[hypo->GetNextInputIndex() + 1] : 0;
	q2p = hypo->GetNextInputIndex() + 2 < words.size() ? &tags[hypo->GetNextInputIndex() + 2] : 0;
	q3p = hypo->GetNextInputIndex() + 3 < words.size() ? &tags[hypo->GetNextInputIndex() + 3] : 0;


	if (s0c != 0)
	{
	    GetValues(1, s0w, s0p, &s0c->vars[s0c->cat->var], words, tags);


/*	    if (s0c->left != 0 && s0c->right != 0)
	    {
		GetValues(1, s0lw, s0lp, &s0c->left->vars[s0c->left->cat->var], words, tags);
		s0lc = s0c->left;
	    }
	    else
	    {
		s0lc = 0;
	    }

	    if (s0c->right != 0 && s0c->left != 0)
	    {
		GetValues(1, s0rw, s0rp, &s0c->right->vars[s0c->right->cat->var], words, tags);
		s0rc = s0c->right;
	    }
	    else
	    {
		s0rc = 0;
	    }*/

	    // s0u

/*	    if (hypo->GetStackTopAction() == UNARY)
	    {
		assert(s0c->left != 0);
		s0uc = s0c->left->cat;
		GetValues(1, s0uw, s0up, &s0c->left->vars[s0c->left->cat->var], words, tags);
	    }
	    else
	    {
		s0uc = 0;
	    }*/

	}


	if (s1c != 0)
	{
	    GetValues(1, s1w, s1p, &s1c->vars[s1c->cat->var], words, tags);

	/*    if (s1c->left != 0)
	    {
		GetValues(1, s1lw, s1lp, &s1c->left->vars[s1c->left->cat->var], words, tags);
		s1lc = s1c->left->cat;
	    }
	    else
	    {
		s1lc = 0;
	    }

	    if (s1c->right != 0)
	    {
		GetValues(1, s1rw, s1rp, &s1c->right->vars[s1c->right->cat->var], words, tags);
		s1rc = s1c->right->cat;

	    }
	    else
	    {
		s1rc = 0;
	    }*/

	    // s1u

	  /*  if (hypo->GetPrvStack()->GetStackTopAction() == UNARY)
	    {
		assert(s1c->left != 0);
		s1uc = s1c->left->cat;
		GetValues(1, s1uw, s1up, &s1c->left->vars[s1c->left->cat->var], words, tags);
	    }
	    else
	    {
		s1uc = 0;
	    }*/

	}


	if (s2c != 0)
	{
	    GetValues(true, s2w, s2p, &s2c->vars[s2c->cat->var], words, tags);
	}

	if (s3c != 0)
	{
	    GetValues(true, s3w, s3p, &s3c->vars[s3c->cat->var], words, tags);
	}


    }


    void GetValues(bool getPos, std::vector<Word> &heads, std::vector<Word> &tags,
	    const Variable *var, const Words &wordValues, const Words &tagValues) const
    {
      const Position *const end = var->fillers + Variable::NFILLERS;
      for(const Position *p = var->fillers; p != end && *p != Variable::SENTINEL; ++p){
        if(!*p)
          continue;

        Word value = wordValues[*p - 1];
        if(!value)
          continue;

        heads.push_back(value);
        if (getPos)
        {
            assert(tagValues[*p - 1]);
            tags.push_back(tagValues[*p - 1]);
        }
      }
    }

};


}
}


#endif /* SHIFTREDUCECONTEXT_H_ */
