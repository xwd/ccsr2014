#ifndef SHIFTREDUCELATTICE_H_
#define SHIFTREDUCELATTICE_H_

#include <cstdlib>
#include "std.h"
#include "pool.h"

namespace NLP
{
namespace CCG
{

class ShiftReduceHypothesis;


class ShiftReduceLattice
{

    /*
      void *operator new[](size_t size, Pool *pool) {
	return (void*)pool->alloc(size);
      }
     */

public:
    ShiftReduceLattice(const size_t nwords, const size_t beamSize, const size_t maxunaryactions)
    : m_latticeArray(new ShiftReduceHypothesis*[(nwords * (2 + maxunaryactions) + 2) * beamSize]),
      m_endIndex(0),
      m_prevEndIndex(0){}
    ~ShiftReduceLattice()
    {
	delete[] m_latticeArray;
    }



/*    void *operator new(size_t size, Pool *pool)
    {
	return (void*)pool->alloc(size);
    }

    void operator delete(void *, Pool *pool) {  do nothing  }*/


    void Add(const size_t index, ShiftReduceHypothesis *hypo)
    {
	//std::cerr << "Lattice Add" << std::endl;
	m_latticeArray[index] = hypo;
    }

    void IncrementEndIndex()
    {
	++m_endIndex;
    }

    size_t GetEndIndex() const
    {
	return m_endIndex;
    }

    size_t GetPrevEndIndex() const
    {
	return m_prevEndIndex;
    }

    void SetPrevEndIndex(size_t ind)
    {
	m_prevEndIndex = ind;
    }

    ShiftReduceHypothesis *GetLatticeArray(size_t ind)
    {
	return m_latticeArray[ind];
    }


private:
    ShiftReduceHypothesis **m_latticeArray;
    size_t m_endIndex;
    size_t m_prevEndIndex;
};


}
}


#endif /* SHIFTREDUCELATTICE_H_ */
