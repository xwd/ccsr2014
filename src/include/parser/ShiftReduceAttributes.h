#ifndef SHIFTREDUCEATTRIBUTES_H_
#define SHIFTREDUCEATTRIBUTES_H_


#include "base.h"
#include <cstddef>


namespace NLP
{
  namespace CCG
  {

  class SwpValue;
  class ScValue;
  class SpcValue;
  class SwcValue;
  class QwpValue;
  class ShiftReduceAction;

  // 1

  class SwpAttributes
  {
  public:
      SwpAttributes(void);
      SwpAttributes(SwpAttributes &other);
      ~SwpAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const SwpValue &value, double weight);

      double GetOrUpdateWeight(std::vector<size_t> &ids, bool getOrUpdate, bool correct,
	      const size_t type, const Word wordValue, const Word tagValue, ShiftReduceAction &action);

  private:
      class Impl;
      Impl *_impl;
  };


  class ScAttributes
  {
  public:
      ScAttributes(void);
      ScAttributes(ScAttributes &other);
      ~ScAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const ScValue &value, double weight);

      double GetOrUpdateWeight(std::vector<size_t> &ids, bool getOrUpdate, bool correct,
	      const size_t type, const Cat *cat,
	      ShiftReduceAction &action);

  private:
      class Impl;
      Impl *_impl;
  };


  class SpcAttributes
  {
  public:
      SpcAttributes(void);
      SpcAttributes(SpcAttributes &other);
      ~SpcAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const SpcValue &value, double weight);

      double GetOrUpdateWeight(std::vector<size_t> &ids, bool getOrUpdate, bool correct,
	      const size_t type, const Word tagValue, const Cat *cat,
	      ShiftReduceAction &action);

  private:
      class Impl;
      Impl *_impl;
  };


  class SwcAttributes
  {
  public:
      SwcAttributes(void);
      SwcAttributes(SwcAttributes &other);
      ~SwcAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const SwcValue &value, double weight);

      double GetOrUpdateWeight(std::vector<size_t> &ids, bool getOrUpdate, bool correct,
	      const size_t type, const Word wordValue, const Cat *cat,
	      ShiftReduceAction &action);

  private:
      class Impl;
      Impl *_impl;
  };


  // 2

  class QwpAttributes
  {
  public:
      QwpAttributes(void);
      QwpAttributes(QwpAttributes &other);
      ~QwpAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const QwpValue &value, double weight);

      double GetOrUpdateWeight(std::vector<size_t> &ids, bool getOrUpdate, bool correct, const size_t type,
	      const Word wordValue, const Word tagValue, ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };

/*
  // 3

  class SlurpcAttributes
  {
  public:
      SlurpcAttributes(void);
      SlurpcAttributes(SlurpcAttributes &other);
      ~SlurpcAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct, const size_t type, const Word tagValue, const Cat *cat,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };


  class SlurwcAttributes
  {
  public:
      SlurwcAttributes(void);
      SlurwcAttributes(SlurwcAttributes &other);
      ~SlurwcAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct, const size_t type, const Word wordValue, const Cat *cat,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };


  // 4


  class S0wcS1wcAttributes
  {
  public:
      S0wcS1wcAttributes(void);
      S0wcS1wcAttributes(S0wcS1wcAttributes &other);
      ~S0wcS1wcAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct,
	      const Word wordValueS0, const Cat *catS0, const Word wordValueS1, const Cat *catS1,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0cS1wAttributes
  {
  public:
      S0cS1wAttributes(void);
      S0cS1wAttributes(S0cS1wAttributes &other);
      ~S0cS1wAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct,
	      const Cat *catS0, const Word wordValueS1,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0wS1cAttributes
  {
  public:
      S0wS1cAttributes(void);
      S0wS1cAttributes(S0wS1cAttributes &other);
      ~S0wS1cAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct,
	      const Word wordValueS0, const Cat *catS1,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0cS1cAttributes
  {
  public:
      S0cS1cAttributes(void);
      S0cS1cAttributes(S0cS1cAttributes &other);
      ~S0cS1cAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct,
	      const Cat *catS0, const Cat *catS1,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };


  class SwcQwpAttributes
  {
  public:
      SwcQwpAttributes(void);
      SwcQwpAttributes(SwcQwpAttributes &other);
      ~SwcQwpAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct,
	      const size_t type, const Word wordValueS, const Cat *catS, const Word wordValueQ, const Word tagValueQ,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };



  class ScQwpAttributes
  {
  public:
      ScQwpAttributes(void);
      ScQwpAttributes(ScQwpAttributes &other);
      ~ScQwpAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct,
	      const size_t type, const Cat *catS, const Word wordValueQ, const Word tagValueQ,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };


  class SwcQpAttributes
  {
  public:
      SwcQpAttributes(void);
      SwcQpAttributes(SwcQpAttributes &other);
      ~SwcQpAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct,
	      const size_t type, const Word wordValueS, const Cat *catS, const Word tagValueQ,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };


  class ScQpAttributes
  {
  public:
      ScQpAttributes(void);
      ScQpAttributes(ScQpAttributes &other);
      ~ScQpAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct,
	      const size_t type, const Cat *catS, const Word tagValueQ,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };


  // 5

  class S0wcS1cQ0pAttributes
  {
  public:
      S0wcS1cQ0pAttributes(void);
      S0wcS1cQ0pAttributes(S0wcS1cQ0pAttributes &other);
      ~S0wcS1cQ0pAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct,
	      const Word wordValueS0, const Cat *catS0, const Cat *catS1, const Word tagValueQ0,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0cS1wcQ0pAttributes
  {
  public:
      S0cS1wcQ0pAttributes(void);
      S0cS1wcQ0pAttributes(S0cS1wcQ0pAttributes &other);
      ~S0cS1wcQ0pAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct,
	      const Cat *catS0, const Word wordValueS1, const Cat *catS1, const Word tagValueQ0,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0cS1cQ0wpAttributes
  {
  public:
      S0cS1cQ0wpAttributes(void);
      S0cS1cQ0wpAttributes(S0cS1cQ0wpAttributes &other);
      ~S0cS1cQ0wpAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct,
	      const Cat *catS0, const Cat *catS1, const Word wordValueQ0, const Word tagValueQ0,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0cS1cQ0pAttributes
  {
  public:
      S0cS1cQ0pAttributes(void);
      S0cS1cQ0pAttributes(S0cS1cQ0pAttributes &other);
      ~S0cS1cQ0pAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct,
	      const Cat *catS0, const Cat *catS1, const Word tagValueQ0,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0pSQpQSpAttributes
  {
  public:
      S0pSQpQSpAttributes(void);
      S0pSQpQSpAttributes(SpSQpQSpAttributes &other);
      ~SpSQpQSpAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct,
	      const size_t type, const Word tagValueS0, const Word tagValueSQ, const Word tagValueQS,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0wcQ0pQ1pAttributes
  {
  public:
      S0wcQ0pQ1pAttributes(void);
      S0wcQ0pQ1pAttributes(S0wcQ0pQ1pAttributes &other);
      ~S0wcQ0pQ1pAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct,
	      const Word wordValueS0, const Cat *catS0, const Word tagValueQ0, const Word tagValueQ1,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0cQ0wpQ1pAttributes
  {
  public:
      S0cQ0wpQ1pAttributes(void);
      S0cQ0wpQ1pAttributes(S0cQ0wpQ1pAttributes &other);
      ~S0cQ0wpQ1pAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct,
	      const Cat *catS0, const Word wordValueQ0, const Word tagValueQ0, const Word tagValueQ1,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0cQ0pQ1wpAttributes
  {
  public:
      S0cQ0pQ1wpAttributes(void);
      S0cQ0pQ1wpAttributes(S0cQ0pQ1wpAttributes &other);
      ~S0cQ0pQ1wpAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct,
	      const Cat *catS0, const Word tagValueQ0, const Word wordValueQ1, const Word tagValueQ1,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0cQ0pQ1pAttributes
  {
  public:
      S0cQ0pQ1pAttributes(void);
      S0cQ0pQ1pAttributes(S0cQ0pQ1pAttributes &other);
      ~S0cQ0pQ1pAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct,
	      const Cat *catS0, const Word tagValueQ0, const Word tagValueQ1,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };



  class S0wcS1cS2cAttributes
  {
  public:
      S0wcS1cS2cAttributes(void);
      S0wcS1cS2cAttributes(S0wcS1cS2cAttributes &other);
      ~S0wcS1cS2cAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct,
	      const Word wordValueS0, const Cat *catS0, const Cat *catS1, const Cat *catS2,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0cS1wcS2cAttributes
  {
  public:
      S0cS1wcS2cAttributes(void);
      S0cS1wcS2cAttributes(S0cS1wcS2cAttributes &other);
      ~S0cS1wcS2cAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct,
	      const Cat *catS0, const Word wordValueS1, const Cat *catS1, const Cat *catS2,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0cS1cS2wcAttributes
  {
  public:
      S0cS1cS2wcAttributes(void);
      S0cS1cS2wcAttributes(S0cS1cS2wcAttributes &other);
      ~S0cS1cS2wcAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct,
	      const Cat *catS0, const Cat *catS1, const Word wordValueS2, const Cat *catS2,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0cS1cS2cAttributes
  {
  public:
      S0cS1cS2cAttributes(void);
      S0cS1cS2cAttributes(S0cS1cS2cAttributes &other);
      ~S0cS1cS2cAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct,
	      const Cat *catS0, const Cat *catS1, const Cat *catS2,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };


  // 6

  class S01cS01LcS01rcAttributes
  {
  public:
      S01cS01LcS01rcAttributes(void);
      S01cS01LcS01rcAttributes(S01cS01LcS01rcAttributes &other);
      ~S01cS01LcS01rcAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct,
	      const size_t type, Cat *cat0, Cat *cat1, Cat *cat2,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0cS0RcQ0pAttributes
  {
  public:
      S0cS0RcQ0pAttributes(void);
      S0cS0RcQ0pAttributes(S0cS0RcQ0pAttributes &other);
      ~S0cS0RcQ0pAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct,
	      Cat *S0c, Cat *S0Rc, Word Q0p,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };

  class S0cSLRcQ0S1wAttributes
  {
  public:
      S0cSLRcQ0S1wAttributes(void);
      S0cSLRcQ0S1wAttributes(S0cSLRcQ0S1wAttributes &other);
      ~S0cSLRcQ0S1wAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct,
	      const size_t type, Cat *S0c, Cat *S0LRc, Word Q0S1w,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0cS0LS1cS1S1RcAttributes
  {
  public:
      S0cS0LS1cS1S1RcAttributes(void);
      S0cS0LS1cS1S1RcAttributes(S0cS0LS1cS1S1RcAttributes &other);
      ~S0cS0LS1cS1S1RcAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct,
	      const size_t type, Cat *cat0, Cat *cat1, Cat *cat2,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };




  class S0wS1cS1RcAttributes
  {
  public:
      S0wS1cS1RcAttributes(void);
      S0wS1cS1RcAttributes(S0wS1cS1RcAttributes &other);
      ~S0wS1cS1RcAttributes(void);

      size_t size(void) const;

      double GetOrUpdateWeight(bool train, bool correct,
	      Word S0w, Cat *S1c, Cats *S1Rc,
	      ShiftReduceAction &action, double currentSent, double totalSent, double currentIter);
  private:
      class Impl;
      Impl *_impl;
  };
*/
  }
}




#endif /* SHIFTREDUCEATTRIBUTES_H_ */
