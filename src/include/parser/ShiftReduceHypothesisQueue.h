#ifndef SHIFTREDUCEHYPOTHESISQUEUE_H_
#define SHIFTREDUCEHYPOTHESISQUEUE_H_

#include "parser/ShiftReduceHypothesis.h"

#include <queue>
#include <vector>


namespace NLP
{
namespace CCG
{

class HypothesisTuple
{
    ShiftReduceHypothesis *m_currentHypo;
    ShiftReduceAction *m_action;
    double m_hypoTotalScore;
public:
    HypothesisTuple(ShiftReduceHypothesis *currentHypo, ShiftReduceAction &action, double hypoTotalScore)
    : m_currentHypo(currentHypo),
      m_action(&action),
      m_hypoTotalScore(hypoTotalScore)
      {}
    ~HypothesisTuple() {}

    ShiftReduceHypothesis *GetCurrentHypo () const
    {
	return m_currentHypo;
    }

    ShiftReduceAction *GetAction() const
    {
	return m_action;
    }

    double GetHypoTotalScore() const
    {
	return m_hypoTotalScore;
    }

};

class HypothesisTupleOrderer
{
public:
    bool operator()(const HypothesisTuple *A, const HypothesisTuple *B) const
    {
	return A->GetHypoTotalScore() < B->GetHypoTotalScore();
    }
};


typedef std::priority_queue<HypothesisTuple*, std::vector<HypothesisTuple*>,
	HypothesisTupleOrderer> HypothesisPQueue;

}
}



#endif /* SHIFTREDUCEHYPOTHESISQUEUE_H_ */
