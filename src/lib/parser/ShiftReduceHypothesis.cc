#include "parser/ShiftReduceHypothesis.h"
#include "parser/ShiftReduceLattice.h"
#include "parser/_parser.h"




namespace NLP
{
namespace CCG
{

class SuperCat;

void ShiftReduceHypothesis::Shift(size_t index, SuperCat *superCat,
	ShiftReduceLattice *lattice, Pool *pool, size_t unaryAcitonCount, double totalScore)
{
    size_t nextInputIndex = m_nextInputIndex + 1;
    superCat->SetInputIndex(m_nextInputIndex);
    ShiftReduceHypothesis *newHypo
    = new ShiftReduceHypothesis(superCat, this, this,
	    nextInputIndex, unaryAcitonCount, ++m_stackSize, totalScore, m_finished, SHIFT, ++m_totalActionCount);
    lattice->Add(index, newHypo);
}


void ShiftReduceHypothesis::Unary(size_t index, SuperCat *superCat,
	ShiftReduceLattice *lattice, Pool *pool, size_t unaryAcitonCount, double totalScore)
{
    //assert(superCat->left);
    //superCat->SetLeftMax();
    //superCat->SetInputIndex(m_nextInputIndex);
    ShiftReduceHypothesis *newHypo
    = new  ShiftReduceHypothesis(superCat, this,
	    m_prevStack, m_nextInputIndex, unaryAcitonCount, m_stackSize, totalScore, m_finished, UNARY, ++m_totalActionCount);
    lattice->Add(index, newHypo);
}


void ShiftReduceHypothesis::Combine(size_t index, SuperCat *superCat,
	ShiftReduceLattice *lattice, Pool *pool, size_t unaryAcitonCount, double totalScore)
{
    //std::cerr << "left: " << superCat->left << std::endl;
    // left and right supercats are set already by candc, but max of them are not set
    // since chart parser uses Viterbi
    //superCat->SetLeftMax();
    //superCat->SetRightMax();
    assert(superCat);
    ShiftReduceHypothesis *newHypo
    = new  ShiftReduceHypothesis(superCat, this, m_prevStack->GetPrvStack(),
	    m_nextInputIndex, unaryAcitonCount, --m_stackSize, totalScore, m_finished, COMBINE, ++m_totalActionCount);
    lattice->Add(index, newHypo);
}


void ShiftReduceHypothesis::Finish(size_t index, ShiftReduceLattice *lattice)
{
    m_finished = true;
    lattice->Add(index, this);
}


} // namespace CCG
} // namespace NLP



