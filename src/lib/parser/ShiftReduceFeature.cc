#include "parser/ShiftReduceFeature.h"
#include "parser/ShiftReduceContext.h"
#include "tree/_baseimpl.h"

#include "parser/categories.h"

#include<iostream>
#include<fstream>

using namespace std;


namespace NLP
{
namespace CCG
{

using namespace NLP::HashTable;
using namespace NLP::Tree;

double ShiftReduceFeature::GetOrUpdateWeight(std::vector<size_t> &ids, bool update, bool correct, ShiftReduceContext &context, ShiftReduceAction &action,
						const Words &words, const Words &tags)
{

    double weight = 0.0;

    if (context.s0c != 0)
    {
	std::vector<Word>::iterator iterw;
	std::vector<Word>::iterator iterp;
	for (iterw = context.s0w.begin(), iterp = context.s0p.begin();
		iterw != context.s0w.end(), iterp != context.s0p.end(); ++iterw, ++iterp)
	{
	    // 1
	    Word s0w = *iterw;
	    Word s0p = *iterp;
	    weight += swpAttrs.GetOrUpdateWeight(ids, update, correct, s0wp, s0w, s0p, action);
	    weight += swcAttrs.GetOrUpdateWeight(ids, update, correct, s0wc, s0w, context.s0c->cat, action);


/*
	    double SwcAttributes::GetOrUpdateWeight(std::vector<size_t> &ids, bool update, bool correct,
	    	const size_t type, const Word wordValue, const Cat *cat,
	    	ShiftReduceAction &action)*/


	    /*
	    // 4
	    if (context.q0w != 0)
	    {
		weight += swcqwpAttrs.GetOrUpdateWeight(train, correct, s0wcq0wp, s0w, s0c, context.q0w, context.q0p, action, currentSent, totalSent, currentIter);
		weight += swcqpAttrs.GetOrUpdateWeight(train, correct, s0wcQp, s0w, context.s0c, context.q0p, action, currentSent, totalSent, currentIter);
	    }


	    // 4
	    if (context.s1c != 0)
	    {
		weight += s0ws1cAttrs.GetOrUpdateWeight(train, correct, s0w, context.s1c, action, currentSent, totalSent, currentIter);
		for (std::vector<Word>::iterator iter = context.s1w.begin(); iter != s1w.end(); ++iter)
		{
		    Word s1w = *iter;
		    weight += s0wcs1wcAttrs.GetOrUpdateWeight(train, correct, s0w, s0c, s1w, s1c, action, currentSent, totalSent, currentIter);
		}
	    }


	    // 5
	    if (context.s1c != 0 && context.q0p != 0) {
		weight += s0wcs1cq0pAttrs.GetOrUpdateWeight(train, correct, s0wcs1cq0p, s0w, context.s0c, context.s1c, context.q0p, action, currentSent, totalSent, currentIter);
		if (context.q1p != 0)
		{
		    weight += s0wcq0pq1pAttrs.GetOrUpdateWeight(train, correct, s0wcq0pq1p, s0w, context.s0c, context.q0p, context.q1p, action, currentSent, totalSent, currentIter);
		}
	    }

	    if (context.s1c =! 0 && context.s2c != 0)
		weight += s0wcs1cs2cAttrs.GetOrUpdateWeight(train, correct, s0wcs1cs2c, s0w, context.s1c, context.s2c, action, currentSent, totalSent, currentIter);


	    // 6
	    if (context.s1c != 0 && context.s1rc != 0)
		weight += s0wS1cS1RcAttrs.GetOrUpdateWeight(train, correct, s02s1cs1rc, s0w, context.s1c, context.s1rc, action, currentSent, totalSent, currentIter);
	    */
	}

	/*
	// 5
	if (context.q0w != 0 && context.q1w != 0)
	{
	    weight += s0cq0wpq1pAttrs.GetOrUpdateWeight(train, correct, s0cq0wpq1p, context.s0c, context.q0p, context.q0w, context.q1p, action, currentSent, totalSent, currentIter);
	    weight += s0cq0wpq1pAttrs.GetOrUpdateWeight(train, correct, s0cq0pq1wp, context.s0c, context.q0p, context.q1w, context.q1p, action, currentSent, totalSent, currentIter);
	    weight += s0cq0pq1pAttrs.GetOrUpdateWeight(train, correct, s0cq0pq1p, context.s0c, context.q0p, context.q1p, action, currentSent, totalSent, currentIter);
	}
	 */


	// 1
	weight += scAttrs.GetOrUpdateWeight(ids, update, correct, s0c_, context.s0c->cat, action);

	for (std::vector<Word>::iterator iter = context.s0p.begin(); iter != context.s0p.end(); ++iter)
	{
	    Word tagValueS0 = *iter;
	    weight += spcAttrs.GetOrUpdateWeight(ids, update, correct, s0pc, tagValueS0, context.s0c->cat, action);
	}

/*	// 4
	if (context.q0w != 0)
	{
	    weight += scqwpAttrs.GetOrUpdateWeight(train, correct, s0cQ0wp, context.s0c, context.q0w, context.q0p, action, currentSent, totalSent, currentIter);
	    weight += scqpAttrs.GetOrUpdateWeight(train, correct, s0cQ0p, context.s0c, context.q0p, action, currentSent, totalSent, currentIter);
	}*/

/*
	// 5

	if (context.q0p != 0 && context.q1p != 0)
	{
	    for (std::vector<Word>::iterator iter = context.s0p.begin(); iter != s0p.end(); ++iter)
	    {
		Word s0p = *iter;
		s0psqpqspAttrs.GetOrUpdateWeight(train, correct, s0pq0pq1p, s0p, context.q0p, context.q1p, action, currentSent, totalSent, currentIter);
	    }
	}



	// 6

	if (context.s0lc != 0 && context.s0rc != 0)
	    s0cScScAttributes.GetOrUpdateWeight(train, correct, s0cs0lcs0rc, context.s0c, context.s0lc, context.s0rc, action, currentSent, totalSent, currentIter);

	if (context.s0rc != 0 && context.q0w != 0)
	    s0cScQ0S1wAttrs.GetOrUpdateWeight(train, correct, s0cs0rcq0w, context.s0c, context.s0rc, context.q0w, action, currentSent, totalSent, currentIter);
*/

    }



    if (context.s1c != 0)
    {
	std::vector<Word>::iterator iterw;
	std::vector<Word>::iterator iterp;
	for (iterw = context.s1w.begin(), iterp = context.s1p.begin();
		iterw != context.s1w.end(), iterp != context.s1p.end(); ++iterw, ++iterp)
	{
	    // 1
	    Word s1w = *iterw;
	    Word s1p = *iterp;
	    weight += swpAttrs.GetOrUpdateWeight(ids, update, correct, s1wp, s1w, s1p, action);
	    weight += swcAttrs.GetOrUpdateWeight(ids, update, correct, s1wc, s1w, context.s1c->cat, action);

	/*    // 4
	    assert(context.s0c != 0);
	    weight += s0cs1wAttrs.GetOrUpdateWeight(train, correct, s0cs1w, context.s0c, s1w, action, currentSent, totalSent, currentIter);

	    if (context.q0w != 0)
	    {
		weight += swcqwpAttrs.GetOrUpdateWeight(train, correct, s1wcq0wp, s1w, s1c, context.q0w, context.q0p, action, currentSent, totalSent, currentIter);
		weight += swcqpAttrs.GetOrUpdateWeight(train, correct, s1wcQp, s1w, context.s1c, context.q0p, action, currentSent, totalSent, currentIter);
	    }

	    // 5
	    assert(context.s0c != 0);
	    if (context.q0p != 0)
		weight += s0cs1cq0pAttrs.GetOrUpdateWeight(train, s0cs1cq0p, correct, context.s0c, context.s1c, context.q0p, action, currentSent, totalSent, currentIter);


	    if (context.s2c != 0)
		weight += s0cs1wcs2cAttrs.GetOrUpdateWeight(train, correct, s0cs1wcs2c, context.s0c, s1w, context.s2c, action, currentSent, totalSent, currentIter);

	    // 6
	    if (context.s0lc != 0)
	    {
		weight += s0cScQ0S1wAttrs.GetOrUpdateWeight(train, correct, s0cs0lcs1w, context.s0c, context.s0lc, s1w, action, currentSent, totalSent, currentIter);
		weight += s0cScScAttrs.GetOrUpdateWeight(train, correct, s0cslcs1c, context.s0c, context.s0lc, context.s1c, action, currentSent, totalSent, currentIter);
	    }

	    if (context.s1rc != 0)
		weight += s0cScScAttrs.GetOrUpdateWeight(train, correct, s0cs1cs1rc, context.s0c, context.s1c, context.s1rc, action, currentSent, totalSent, currentIter);
*/
	}
/*

	// 4
	assert(context.s0c != 0);
	weight += s0cs1cAttrs.GetOrUpdateWeight(train, s0cs1c, context.s0c, context.s1c, action, currentSent, totalSent, currentIter);

*/


	// 1
	weight += scAttrs.GetOrUpdateWeight(ids, update, correct, s1c_, context.s1c->cat, action);

	for (std::vector<Word>::iterator iterp = context.s1p.begin(); iterp != context.s1p.end(); ++iterp)
	{
	    Word s1p = *iterp;
	    weight += spcAttrs.GetOrUpdateWeight(ids, update, correct, s1pc, s1p, context.s1c->cat, action);
	}
/*
	// 4
	if (context.q0w != 0)
	{
	    weight += scqwpAttrs.GetOrUpdateWeight(train, correct, s1cQ0wp, context.s1c, context.q0w, context.q0p, action, currentSent, totalSent, currentIter);
	    weight += scqpAttrs.GetOrUpdateWeight(train, correct, s1cQ0p, context.s1c, context.q0p, action, currentSent, totalSent, currentIter);
	}

	// 5
	if (context.q0p != 0)
	{
	    weight += s0cs1cq0wpAttrs.GetOrUpdateWeight(train, s0cs1cq0wp, correct, context.s0c, context.s1c, context.q0w, context.q0p, action, currentSent, totalSent, currentIter);
	    weight += s0cs1cq0pAttrs.GetOrUpdateWeight(train, s0cs1cq0p, correct, context.s0c, context.s1c, context.q0p, action, currentSent, totalSent, currentIter);
	}


	if (context.q0p != 0)
	{
	    for (std::vector<Word>::iterator iter = context.s0p.begin(); iter != s0p.end(); ++iter)
	    {
		Word s0p = *iter;
		for (std::vector<Word>::iterator iter = context.s1p.begin(); iter != s1p.end(); ++iter)
		{
		    Word s1p = *iter;
		    s0psqpqspAttrs.GetOrUpdateWeight(train, correct, s0ps1pq0p, s0p, s1p, context.q0p, action, currentSent, totalSent, currentIter);
		}
	    }
	}

	// 6
	if (context.s1lc != 0 && context.s1rc != 0)
	    s0cS0RcQ0pAttrs.GetOrUpdateWeight(train, correct, s1cs1lcs1rc, context.s1c, context.s1lc, context.s1rc, action, currentSent, totalSent, currentIter);

  */
    }


    if (context.s2c != 0)
    {
	// 1
	for (std::vector<Word>::iterator iter = context.s2w.begin(); iter != context.s2w.end(); ++iter)
	{
	    Word s2w = *iter;
	    weight += swcAttrs.GetOrUpdateWeight(ids, update, correct, s2wc, s2w, context.s2c->cat, action);
/*
	    // 5
	    weight += s0cs1cs2wcAttrs.GetOrUpdateWeight(train, correct, s0cs2cs2wc, context.s0c, context.s2c, s2w, context.s2c, action, currentSent, totalSent, currentIter);*/
	}

	// 1
	for (std::vector<Word>::iterator iter = context.s2p.begin(); iter != context.s2p.end(); ++iter)
	{
	    Word s2p = *iter;
	    weight += spcAttrs.GetOrUpdateWeight(ids, update, correct, s2pc, s2p, context.s2c->cat, action);
	}

/*	// 5
	weight += s0cs1cs2cAttris.GetOrUpdateWeight(train, correct, s0cs1cs2c, context.s0c, context.s1c, context.s2c, action, currentSent, totalSent, currentIter);

	for (std::vector<Word>::iterator iter = context.s0p.begin(); iter != s0p.end(); ++iter)
	{
	    Word s0p = *iter;
	    for (std::vector<Word>::iterator iter = context.s1p.begin(); iter != s1p.end(); ++iter)
	    {
		Word s1p = *iter;
		for (std::vector<Word>::iterator iter = context.s2p.begin(); iter != s2p.end(); ++iter)
		{
		    Word s2p = *iter;
		    weight += s0ps1ps2pAttrs.GetOrUpdateWeight(train, correct, s0ps1ps2p, s0p, s1p, s2p, action, currentSent, totalSent, currentIter);
		}
	    }
	}*/

    }



    if (context.s3c != 0)
    {

	// 1
	for (std::vector<Word>::iterator iter = context.s3w.begin(); iter != context.s3w.end(); ++iter)
	{
	    Word s3w = *iter;
	    weight += swcAttrs.GetOrUpdateWeight(ids, update, correct, s3wc, s3w, context.s3c->cat, action);
	}


	for (std::vector<Word>::iterator iter = context.s3p.begin(); iter != context.s3p.end(); ++iter)
	{
	    Word s3p = *iter;
	    weight += spcAttrs.GetOrUpdateWeight(ids, update, correct, s3pc, s3p, context.s3c->cat, action);
	}
    }


    // 2
    if (context.q0w != 0)
    {
	weight += qwpAttrs.GetOrUpdateWeight(ids, update, correct, q0wp, *context.q0w, *context.q0p, action);
    }

    if (context.q1w != 0)
    {
	weight += qwpAttrs.GetOrUpdateWeight(ids, update, correct, q1wp, *context.q1w, *context.q1p, action);
    }

    if (context.q2w != 0)
    {
	weight += qwpAttrs.GetOrUpdateWeight(ids, update, correct, q2wp, *context.q2w, *context.q2p, action);
    }

    if (context.q3w != 0)
    {
	weight += qwpAttrs.GetOrUpdateWeight(ids, update, correct, q3wp, *context.q3w, *context.q3p, action);
    }

/*
    // 3
    if (context.s0lc != 0)
    {
	for (std::vector<Word>::iterator iter = context.s0lp.begin(); iter != context.s0lp.end(); ++iter)
	{
	    Word s0p = *iter;
	    weight += slurpcAttrs.GetOrUpdateWeight(train, correct, s0lpc, s0p, context.s0lc, action, currentSent, totalSent, currentIter);
	}

	for (std::vector<Word>::iterator iter = context.s0lw.begin(); iter != context.s0lw.end(); ++iter)
	{
	    Word s0lw = *iter;
	    weight += slurwcAttrs.GetOrUpdateWeight(train, correct, s0lwc, s0lw, context.s0lc, action, currentSent, totalSent, currentIter);
	}
    }


    // 3
    if (context.s0rc != 0)
    {
	for (std::vector<Word>::iterator iter = context.s0rp.begin(); iter != context.s0rp.end(); ++iter)
	{
	    Word tag = *iter;
	    weight += slurpcAttrs.GetOrUpdateWeight(train, correct, s0rpc, tag, context.s0rc, action, currentSent, totalSent, currentIter);
	}

	for (std::vector<Word>::iterator iter = context.s0rw.begin(); iter != context.s0rw.end(); ++iter)
	{
	    Word word = *iter;
	    weight += slurwcAttrs.GetOrUpdateWeight(train, correct, s0rwc, word, context.s0rc, action, currentSent, totalSent, currentIter);
	}
    }

    // 3
    if (context.s1lc != 0)
    {
	for (std::vector<Word>::iterator iter = context.s1lp.begin(); iter != context.s1lp.end(); ++iter)
	{
	    Word tag = *iter;
	    weight += slurpcAttrs.GetOrUpdateWeight(train, correct, s1lpc, tag, context.s1lc, action, currentSent, totalSent, currentIter);
	}

	for (std::vector<Word>::iterator iter = context.s1lw.begin(); iter != context.s1lw.end(); ++iter)
	{
	    Word word = *iter;
	    weight += slurwcAttrs.GetOrUpdateWeight(train, correct, s1lwc, word, context.s1lc, action, currentSent, totalSent, currentIter);
	}

    }


    // 3
    if (context.s1rc != 0)
    {
	for (std::vector<Word>::iterator iter = context.s1rp.begin(); iter != context.s1rp.end(); ++iter)
	{
	    Word tag = *iter;
	    weight += slurpcAttrs.GetOrUpdateWeight(train, correct, s1rpc, tag, context.s1rc, action, currentSent, totalSent, currentIter);
	}

	for (std::vector<Word>::iterator iter = context.s1rw.begin(); iter != context.s1rw.end(); ++iter)
	{
	    Word word = *iter;
	    weight += slurwcAttrs.GetOrUpdateWeight(train, correct, s1rwc, word, context.s1rc, action, currentSent, totalSent, currentIter);
	}
    }

    // 3
    if (context.s0uc != 0)
    {
	for (std::vector<Word>::iterator iter = context.s0up.begin(); iter != context.s0up.end(); ++iter)
	{
	    Word tag = *iter;
	    weight += slurpcAttrs.GetOrUpdateWeight(train, correct, s0upc, tag, context.s0uc, action, currentSent, totalSent, currentIter);
	}

	for (std::vector<Word>::iterator iter = context.s0uw.begin(); iter != context.s0uw.end(); ++iter)
	{
	    Word word = *iter;
	    weight += slurpcAttrs.GetOrUpdateWeight(train, correct, s0uwc, word, context.s0uc, action, currentSent, totalSent, currentIter);
	}
    }


    // 3
    if (context.s1uc != 0)
    {
	for (std::vector<Word>::iterator iter = context.s1up.begin(); iter != context.s1up.end(); ++iter)
	{
	    Word tag = *iter;
	    weight += slurpcAttrs.GetOrUpdateWeight(train, correct, s1upc, tag, context.s1uc, action, currentSent, totalSent, currentIter);
	}

	for (std::vector<Word>::iterator iter = context.s1uw.begin(); iter != context.s1uw.end(); ++iter)
	{
	    Word word = *iter;
	    weight += slurpcAttrs.GetOrUpdateWeight(train, correct, s1uwc, word, context.s1uc, action, currentSent, totalSent, currentIter);
	}
    }*/

    return weight;
}

const Cat *ShiftReduceFeature::GetCat(std::string tmp)
{
    const Cat *cat = cats.markedup[tmp];
    if(!cat)
    {
	try
	{
	    cat = cats.parse(tmp);
	}
	catch(NLP::Exception e)
	{
	    cerr << "maxent.exception: " << e.what() << endl;
	    cerr << "attempting to parse category " << tmp  << endl;
	}
    }

    return cat;
}



void ShiftReduceFeature::SetWeights(istream &in, size_t type)
{

      switch(type)
    {

    case 1:
    case 2:
    {

	string word;
	string tag;

	size_t actionId;
	string acatStr;

	double weight;

	in >> word >> tag >> actionId >> acatStr >> weight;

	Word wordValue = lexicon[word];
	Word tagValue = lexicon[tag];

	const Cat *acat = GetCat(acatStr);
	ShiftReduceAction action(actionId, acat);

	SwpValue swpValue(type, wordValue, tagValue, action);
	swpAttrs.SetFeature(swpValue, weight); break;
    }
    case 3:
    case 4:
    {
	string fcatStr;

	size_t actionId;
	string acatStr;

	double weight;

	in >> fcatStr >> actionId >> acatStr >> weight;

	const Cat *fcat = GetCat(fcatStr);
	const Cat *acat = GetCat(acatStr);
	ShiftReduceAction action(actionId, acat);
	ScValue scValue(type, fcat, action);
	scAttrs.SetFeature(scValue, weight); break;
    }

    case 5:
    case 6:
    case 7:
    case 8:
    {

	string tag;
	string fcatStr;

	size_t actionId;
	string acatStr;

	double weight;

	in >> tag >> fcatStr >> actionId >> acatStr >> weight;
	const Cat *fcat = GetCat(fcatStr);
	const Cat *acat = GetCat(acatStr);

	Word tagValue = lexicon[tag];

	ShiftReduceAction action(actionId, acat);
	SpcValue spcValue(type, tagValue, fcat, action);
	spcAttrs.SetFeature(spcValue, weight); break;
    }
    case 9:
    case 10:
    case 11:
    case 12:
    {

	string word;
	string fcatStr;

	size_t actionId;
	string acatStr;

	double weight;

	in >> word >> fcatStr >> actionId >> acatStr >> weight;
	const Cat *fcat = GetCat(fcatStr);
	const Cat *acat = GetCat(acatStr);

	Word wordValue = lexicon[word];
	ShiftReduceAction action(actionId, acat);
	SwcValue swcValue(type, wordValue, fcat, action);
	swcAttrs.SetFeature(swcValue, weight); break;
    }

    case 13:
    case 14:
    case 15:
    case 16:
    {

	string word;
	string tag;

	size_t actionId;
	string acatStr;

	double weight;

	in >> word >> tag >> actionId >> acatStr >> weight;

	Word wordValue = lexicon[word];
	Word tagValue = lexicon[tag];

	const Cat *acat = GetCat(acatStr);
	ShiftReduceAction action(actionId, acat);

	QwpValue qwpValue(type, wordValue, tagValue, action);
	qwpAttrs.SetFeature(qwpValue, weight); break;
    }

    default:
	throw NLP::IOException("unexpected feature type in feature loading, ShiftReduceFeature::SetWeight()");
    }

}



bool ShiftReduceFeature::SaveWeights(std::ofstream &out)
{
    // 1
    swpAttrs.SaveWeights(out);
    scAttrs.SaveWeights(out);
    spcAttrs.SaveWeights(out);
    swcAttrs.SaveWeights(out);

    // 2
    qwpAttrs.SaveWeights(out);

    return true;

}


// 1


class SwpAttributes::Impl: public AttributesImpl<SwpValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<SwpValue, MEDIUM, LARGE>("SwpAttributes"){}
};


SwpAttributes::SwpAttributes(void): _impl(new Impl) {}
SwpAttributes::SwpAttributes(SwpAttributes &other): _impl(share(other._impl)) {}
SwpAttributes::~SwpAttributes(void){ release(_impl); }

size_t SwpAttributes::size(void) const { return _impl->size; }
void SwpAttributes::SetFeature(const SwpValue &value, double weight)
{
    _impl->insertSR(value, weight);
}

void SwpAttributes::SaveWeights(std::ofstream &out)
{
    _impl->SaveWeights(out);
}

double SwpAttributes::GetOrUpdateWeight(std::vector<size_t> &ids, bool update, bool correct,
	const size_t type, const Word wordValue, const Word tagValue,
	ShiftReduceAction &action)
{
    double weight = 0.0;

    SwpValue swpValue(type, wordValue, tagValue, action);
    if (update)
    {
	_impl->PerceptronUpdate(ids, correct, swpValue);
    }
    else
    {
	return _impl->find_weight(swpValue);
    }

    return weight;
}

//double S0cAttributes::weight(const Cat *cat, Word value) const {
//CatValue catvalue(cat, value);
//return _impl->find_weight(catvalue);
//}


class ScAttributes::Impl: public AttributesImpl<ScValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<ScValue, MEDIUM, LARGE>("ScAttributes"){}
};


ScAttributes::ScAttributes(void): _impl(new Impl) {}
ScAttributes::ScAttributes(ScAttributes &other): _impl(share(other._impl)) {}
ScAttributes::~ScAttributes(void){ release(_impl); }

size_t ScAttributes::size(void) const { return _impl->size; }
void ScAttributes::SetFeature(const ScValue &value, double weight){
    _impl->insertSR(value, weight);
}

void ScAttributes::SaveWeights(std::ofstream &out)
{
    _impl->SaveWeights(out);
}

double ScAttributes::GetOrUpdateWeight(std::vector<size_t> &ids, bool update, bool correct,
	const size_t type, const Cat *cat,
	ShiftReduceAction &action)
{
    double weight = 0.0;

    ScValue scValue(type, cat, action);
    if (update)
    {
	_impl->PerceptronUpdate(ids, correct, scValue);
    }
    else
    {
	return _impl->find_weight(scValue);
    }

    return weight;
}


class SpcAttributes::Impl: public AttributesImpl<SpcValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<SpcValue, MEDIUM, LARGE>("SpcAttributes"){}
};


SpcAttributes::SpcAttributes(void): _impl(new Impl) {}
SpcAttributes::SpcAttributes(SpcAttributes &other): _impl(share(other._impl)) {}
SpcAttributes::~SpcAttributes(void){ release(_impl); }

size_t SpcAttributes::size(void) const { return _impl->size; }
void SpcAttributes::SetFeature(const SpcValue &value, double weight){
    _impl->insertSR(value, weight);
}

void SpcAttributes::SaveWeights(std::ofstream &out)
{
    _impl->SaveWeights(out);
}

double SpcAttributes::GetOrUpdateWeight(std::vector<size_t> &ids, bool update, bool correct,
	const size_t type, const Word tagValue, const Cat *cat,
	ShiftReduceAction &action)
{
    double weight = 0.0;

    SpcValue spcValue(type, tagValue, cat, action);
    if (update)
    {
	_impl->PerceptronUpdate(ids, correct, spcValue);
    }
    else
    {
	return _impl->find_weight(spcValue);
    }

    return weight;
}


class SwcAttributes::Impl: public AttributesImpl<SwcValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<SwcValue, MEDIUM, LARGE>("SwcAttributes"){}
};


SwcAttributes::SwcAttributes(void): _impl(new Impl) {}
SwcAttributes::SwcAttributes(SwcAttributes &other): _impl(share(other._impl)) {}
SwcAttributes::~SwcAttributes(void){ release(_impl); }

size_t SwcAttributes::size(void) const { return _impl->size; }
void SwcAttributes::SetFeature(const SwcValue &value, double weight){
    _impl->insertSR(value, weight);
}

void SwcAttributes::SaveWeights(std::ofstream &out)
{
    _impl->SaveWeights(out);
}

double SwcAttributes::GetOrUpdateWeight(std::vector<size_t> &ids, bool update, bool correct,
	const size_t type, const Word wordValue, const Cat *cat,
	ShiftReduceAction &action)
{

    double weight = 0.0;

    SwcValue swcValue(type, wordValue, cat, action);
    if (update)
    {
	_impl->PerceptronUpdate(ids, correct, swcValue);
    }
    else
    {
	weight = _impl->find_weight(swcValue);
    }

    return weight;
}


// 2

class QwpAttributes::Impl: public AttributesImpl<QwpValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<QwpValue, MEDIUM, LARGE>("QwpAttributes"){}
};


QwpAttributes::QwpAttributes(void): _impl(new Impl) {}
QwpAttributes::QwpAttributes(QwpAttributes &other): _impl(share(other._impl)) {}
QwpAttributes::~QwpAttributes(void){ release(_impl); }

size_t QwpAttributes::size(void) const { return _impl->size; }
void QwpAttributes::SetFeature(const QwpValue &value, double weight){
    _impl->insertSR(value, weight);
}

void QwpAttributes::SaveWeights(std::ofstream &out)
{
    _impl->SaveWeights(out);
}

double QwpAttributes::GetOrUpdateWeight(std::vector<size_t> &ids, bool update, bool correct,
	const size_t type, const Word wordValue, const Word tagValue,
	ShiftReduceAction &action)
{

    double weight = 0.0;
    QwpValue qwpValue(type, wordValue, tagValue, action);
    if (update)
    {
	_impl->PerceptronUpdate(ids, correct, qwpValue);
    }
    else
    {
	weight = _impl->find_weight(qwpValue);
    }

    return weight;
}


/*
// 3

class SlurpcAttributes::Impl: public AttributesImpl<SlurpcValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<SlurpcValue, MEDIUM, LARGE>("SlurpcAttributes"){}
};

SlurpcAttributes::SlurpcAttributes(void): _impl(new Impl) {}
SlurpcAttributes::SlurpcAttributes(SlurpcAttributes &other): _impl(share(other._impl)) {}
SlurpcAttributes::~SlurpcAttributes(void){ release(_impl); }

size_t SlurpcAttributes::size(void) const { return _impl->size; }
void SlurpcAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double SlurpcAttributes::GetOrUpdateWeight(bool train, bool correct, const size_t type, const Word tagValue, const Cat *cat,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    SlurpcValue slurpcValue(type, tagValue, cat, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, slurpcValue, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(slurpcValue);
    }
}


class SlurwcAttributes::Impl: public AttributesImpl<SlurwcValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<SlurwcValue, MEDIUM, LARGE>("SlurwcAttributes"){}
};

SlurwcAttributes::SlurwcAttributes(void): _impl(new Impl) {}
SlurwcAttributes::SlurwcAttributes(SlurwcAttributes &other): _impl(share(other._impl)) {}
SlurwcAttributes::~SlurwcAttributes(void){ release(_impl); }

size_t SlurwcAttributes::size(void) const { return _impl->size; }
void SlurwcAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double SlurwcAttributes::GetOrUpdateWeight(bool train, bool correct, const size_t type, const Word wordValue, const Cat *cat,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    SlurwcValue slurwcValue(type, wordValue, cat, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, slurwcValue, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(slurwcValue);
    }
}


// 4

class S0wcS1wcAttributes::Impl: public AttributesImpl<S0wcS1wcValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<S0wcS1wcValue, MEDIUM, LARGE>("S0wcS1wcAttributes"){}
};

S0wcS1wcAttributes::S0wcS1wcAttributes(void): _impl(new Impl) {}
S0wcS1wcAttributes::S0wcS1wcAttributes(S0wcS1wcAttributes &other): _impl(share(other._impl)) {}
S0wcS1wcAttributes::~S0wcS1wcAttributes(void){ release(_impl); }

size_t S0wcS1wcAttributes::size(void) const { return _impl->size; }
void S0wcS1wcAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double S0wcS1wcAttributes::GetOrUpdateWeight(bool train, bool correct,
	const Word wordValueS0, const Cat *catS0, const Word wordValueS1, const Cat *catS1,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    S0wcS1wcValue s0wcS1wcValue(wordValueS0, catS0, wordValueS1, catS1, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, s0wcS1wcValue, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(s0wcS1wcValue);
    }
}


class S0cS1wAttributes::Impl: public AttributesImpl<S0cS1wValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<S0cS1wValue, MEDIUM, LARGE>("S0cS1wAttributes"){}
};

S0cS1wAttributes::S0cS1wAttributes(void): _impl(new Impl) {}
S0cS1wAttributes::S0cS1wAttributes(S0cS1wAttributes &other): _impl(share(other._impl)) {}
S0cS1wAttributes::~S0cS1wAttributes(void){ release(_impl); }

size_t S0cS1wAttributes::size(void) const { return _impl->size; }
void S0cS1wAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double S0cS1wAttributes::GetOrUpdateWeight(bool train, bool correct,
	const Cat *catS0, const Word wordValueS1,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    S0cS1wValue s0cS1wValue(catS0, wordValueS1, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, s0cS1wValue, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(s0cS1wValue);
    }
}


class S0wS1cAttributes::Impl: public AttributesImpl<S0wS1cValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<S0wS1cValue, MEDIUM, LARGE>("S0wS1cAttributes"){}
};

S0wS1cAttributes::S0wS1cAttributes(void): _impl(new Impl) {}
S0wS1cAttributes::S0wS1cAttributes(S0wS1cAttributes &other): _impl(share(other._impl)) {}
S0wS1cAttributes::~S0wS1cAttributes(void){ release(_impl); }

size_t S0wS1cAttributes::size(void) const { return _impl->size; }
void S0wS1cAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double S0wS1cAttributes::GetOrUpdateWeight(bool train, bool correct,
	const Word wordValueS0, const Cat *catS1,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    S0wS1cValue s0wS1cValue(wordValueS0, catS1, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, s0wS1cValue, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(s0wS1cValue);
    }
}


class S0cS1cAttributes::Impl: public AttributesImpl<S0cS1cValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<S0cS1cValue, MEDIUM, LARGE>("S0cS1cAttributes"){}
};

S0cS1cAttributes::S0cS1cAttributes(void): _impl(new Impl) {}
S0cS1cAttributes::S0cS1cAttributes(S0cS1cAttributes &other): _impl(share(other._impl)) {}
S0cS1cAttributes::~S0cS1cAttributes(void){ release(_impl); }

size_t S0cS1cAttributes::size(void) const { return _impl->size; }
void S0cS1cAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double S0cS1cAttributes::GetOrUpdateWeight(bool train, bool correct,
	const Cat *catS0, const Cat *catS1,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    S0cS1cValue s0cS1cValue(catS0, catS1, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, s0cS1cValue, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(s0cS1cValue);
    }
}


class SwcQwpAttributes::Impl: public AttributesImpl<SwcQwpValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<SwcQwpValue, MEDIUM, LARGE>("SwcQwpAttributes"){}
};

SwcQwpAttributes::SwcQwpAttributes(void): _impl(new Impl) {}
SwcQwpAttributes::SwcQwpAttributes(SwcQwpAttributes &other): _impl(share(other._impl)) {}
SwcQwpAttributes::~SwcQwpAttributes(void){ release(_impl); }

size_t SwcQwpAttributes::size(void) const { return _impl->size; }
void SwcQwpAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double SwcQwpAttributes::GetOrUpdateWeight(bool train, bool correct,
	const size_t type, const Word wordValueS, const Cat *catS, const Word wordValueQ, const Word tagValueQ,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    SwcQwpValue swcQwpValue(type, wordValueS, catS, wordValueQ, tagValueQ, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, swcQwpValue, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(swcQwpValue);
    }
}


class ScQwpAttributes::Impl: public AttributesImpl<ScQwpValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<ScQwpValue, MEDIUM, LARGE>("ScQwpAttributes"){}
};

ScQwpAttributes::ScQwpAttributes(void): _impl(new Impl) {}
ScQwpAttributes::ScQwpAttributes(ScQwpAttributes &other): _impl(share(other._impl)) {}
ScQwpAttributes::~ScQwpAttributes(void){ release(_impl); }

size_t ScQwpAttributes::size(void) const { return _impl->size; }
void ScQwpAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double ScQwpAttributes::GetOrUpdateWeight(bool train, bool correct,
	const size_t type, const Cat *catS, const Word wordValueQ, const Word tagValueQ,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    ScQwpValue scQwpValue(type, catS, wordValueQ, tagValueQ, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, scQwpValue, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(scQwpValue);
    }
}


class SwcQpAttributes::Impl: public AttributesImpl<SwcQpValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<SwcQpValue, MEDIUM, LARGE>("SwcQpAttributes"){}
};

SwcQpAttributes::SwcQpAttributes(void): _impl(new Impl) {}
SwcQpAttributes::SwcQpAttributes(SwcQpAttributes &other): _impl(share(other._impl)) {}
SwcQpAttributes::~SwcQpAttributes(void){ release(_impl); }

size_t SwcQpAttributes::size(void) const { return _impl->size; }
void SwcQpAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double SwcQpAttributes::GetOrUpdateWeight(bool train, bool correct,
	const size_t type, const Word wordValueS, const Cat *catS, const Word tagValueQ,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    SwcQpValue swcQpValue(type, wordValueS, catS, tagValueQ, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, swcQpValue, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(swcQpValue);
    }
}



class ScQpAttributes::Impl: public AttributesImpl<ScQpValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<ScQpValue, MEDIUM, LARGE>("ScQpAttributes"){}
};

ScQpAttributes::ScQpAttributes(void): _impl(new Impl) {}
ScQpAttributes::ScQpAttributes(ScQpAttributes &other): _impl(share(other._impl)) {}
ScQpAttributes::~ScQpAttributes(void){ release(_impl); }

size_t ScQpAttributes::size(void) const { return _impl->size; }
void ScQpAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double ScQpAttributes::GetOrUpdateWeight(bool train, bool correct,
	const size_t type, const Cat *catS, const Word tagValueQ,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    ScQpValue scQpValue(type, catS, tagValueQ, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, scQpValue, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(scQpValue);
    }
}


// 5

class S0wcS1cQ0pAttributes::Impl: public AttributesImpl<S0wcS1cQ0pValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<S0wcS1cQ0pValue, MEDIUM, LARGE>("S0wcS1cQ0pAttributes"){}
};

S0wcS1cQ0pAttributes::S0wcS1cQ0pAttributes(void): _impl(new Impl) {}
S0wcS1cQ0pAttributes::S0wcS1cQ0pAttributes(S0wcS1cQ0pAttributes &other): _impl(share(other._impl)) {}
S0wcS1cQ0pAttributes::~S0wcS1cQ0pAttributes(void){ release(_impl); }

size_t S0wcS1cQ0pAttributes::size(void) const { return _impl->size; }
void S0wcS1cQ0pAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double S0wcS1cQ0pAttributes::GetOrUpdateWeight(bool train, bool correct,
	const Word wordValueS0, const Cat *catS0, const Cat *catS1, const Word tagValueQ0,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    S0wcS1cQ0pValue s0wcS1cQ0pValue(wordValueS0, catS0, catS1, tagValueQ0, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, s0wcS1cQ0pValue, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(s0wcS1cQ0pValue);
    }
}


class S0cS1wcQ0pAttributes::Impl: public AttributesImpl<S0cS1wcQ0pValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<S0cS1wcQ0pValue, MEDIUM, LARGE>("S0cS1wcQ0pAttributes"){}
};

S0cS1wcQ0pAttributes::S0cS1wcQ0pAttributes(void): _impl(new Impl) {}
S0cS1wcQ0pAttributes::S0cS1wcQ0pAttributes(S0cS1wcQ0pAttributes &other): _impl(share(other._impl)) {}
S0cS1wcQ0pAttributes::~S0cS1wcQ0pAttributes(void){ release(_impl); }

size_t S0cS1wcQ0pAttributes::size(void) const { return _impl->size; }
void S0cS1wcQ0pAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double S0cS1wcQ0pAttributes::GetOrUpdateWeight(bool train, bool correct,
	const Cat *catS0, const Word wordValueS1, const Cat *catS1, const Word tagValueQ0,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    S0cS1wcQ0pValue s0cS1wcQ0pValue(catS0, wordValueS1, catS1, tagValueQ0, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, s0cS1wcQ0pValue, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(s0cS1wcQ0pValue);
    }
}


class S0cS1cQ0wpAttributes::Impl: public AttributesImpl<S0cS1cQ0wpValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<S0cS1cQ0wpValue, MEDIUM, LARGE>("S0cS1cQ0wpAttributes"){}
};

S0cS1cQ0wpAttributes::S0cS1cQ0wpAttributes(void): _impl(new Impl) {}
S0cS1cQ0wpAttributes::S0cS1cQ0wpAttributes(S0cS1cQ0wpAttributes &other): _impl(share(other._impl)) {}
S0cS1cQ0wpAttributes::~S0cS1cQ0wpAttributes(void){ release(_impl); }

size_t S0cS1cQ0wpAttributes::size(void) const { return _impl->size; }
void S0cS1cQ0wpAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double S0cS1cQ0wpAttributes::GetOrUpdateWeight(bool train, bool correct,
	const Cat *catS0, const Cat *catS1, const Word wordValueQ0, const Word tagValueQ0,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    S0cS1cQ0wpValue s0cS1cQ0wpValue(catS0, catS1, wordValueQ0, tagValueQ0, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, s0cS1cQ0wpValue, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(s0cS1cQ0wpValue);
    }
}


class S0cS1cQ0pAttributes::Impl: public AttributesImpl<S0cS1cQ0pValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<S0cS1cQ0pValue, MEDIUM, LARGE>("S0cS1cQ0pAttributes"){}
};

S0cS1cQ0pAttributes::S0cS1cQ0pAttributes(void): _impl(new Impl) {}
S0cS1cQ0pAttributes::S0cS1cQ0pAttributes(S0cS1cQ0pAttributes &other): _impl(share(other._impl)) {}
S0cS1cQ0pAttributes::~S0cS1cQ0pAttributes(void){ release(_impl); }

size_t S0cS1cQ0pAttributes::size(void) const { return _impl->size; }
void S0cS1cQ0pAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double S0cS1cQ0pAttributes::GetOrUpdateWeight(bool train, bool correct,
	const Cat *catS0, const Cat *catS1, const Word tagValueQ0,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    S0cS1cQ0pValue s0cS1cQ0pValue(catS0, catS1, tagValueQ0, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, s0cS1cQ0pValue, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(s0cS1cQ0pValue);
    }
}


class S0pSQpQSpAttributes::Impl: public AttributesImpl<S0pSQpQSpValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<S0pSQpQSpValue, MEDIUM, LARGE>("S0pSQpQSpAttributes"){}
};

S0pSQpQSpAttributes::S0pSQpQSpAttributes(void): _impl(new Impl) {}
S0pSQpQSpAttributes::S0pSQpQSpAttributes(S0pSQpQSpAttributes &other): _impl(share(other._impl)) {}
S0pSQpQSpAttributes::~S0pSQpQSpAttributes(void){ release(_impl); }

size_t S0pSQpQSpAttributes::size(void) const { return _impl->size; }
void S0pSQpQSpAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double S0pSQpQSpAttributes::GetOrUpdateWeight(bool train, bool correct,
	const size_t type, const Word tagValueS0, const Word tagValueSQ, const Word tagValueQS,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    S0pSQpQSpValue s0pSQpQSpValue(type, tagValueS0, tagValueSQ, tagValueQS, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, s0pSQpQSpValue, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(s0pSQpQSpValue);
    }
}


class S0wcQ0pQ1pAttributes::Impl: public AttributesImpl<S0wcQ0pQ1pValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<S0wcQ0pQ1pValue, MEDIUM, LARGE>("S0wcQ0pQ1pAttributes"){}
};

S0wcQ0pQ1pAttributes::S0wcQ0pQ1pAttributes(void): _impl(new Impl) {}
S0wcQ0pQ1pAttributes::S0wcQ0pQ1pAttributes(S0wcQ0pQ1pAttributes &other): _impl(share(other._impl)) {}
S0wcQ0pQ1pAttributes::~S0wcQ0pQ1pAttributes(void){ release(_impl); }

size_t S0wcQ0pQ1pAttributes::size(void) const { return _impl->size; }
void S0wcQ0pQ1pAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double S0wcQ0pQ1pAttributes::GetOrUpdateWeight(bool train, bool correct,
	const Word wordValueS0, const Cat *catS0, const Word tagValueQ0, const Word tagValueQ1,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    S0wcQ0pQ1pValue s0wcQ0pQ1pValue(wordValueS0, catS0, tagValueQ0, tagValueQ1, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, s0wcQ0pQ1pValue, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(s0wcQ0pQ1pValue);
    }
}


class S0cQ0wpQ1pAttributes::Impl: public AttributesImpl<S0cQ0wpQ1pValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<S0cQ0wpQ1pValue, MEDIUM, LARGE>("S0cQ0wpQ1pAttributes"){}
};

S0cQ0wpQ1pAttributes::S0cQ0wpQ1pAttributes(void): _impl(new Impl) {}
S0cQ0wpQ1pAttributes::S0cQ0wpQ1pAttributes(S0cQ0wpQ1pAttributes &other): _impl(share(other._impl)) {}
S0cQ0wpQ1pAttributes::~S0cQ0wpQ1pAttributes(void){ release(_impl); }

size_t S0cQ0wpQ1pAttributes::size(void) const { return _impl->size; }
void S0cQ0wpQ1pAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double S0cQ0wpQ1pAttributes::GetOrUpdateWeight(bool train, bool correct,
	const Cat *catS0, const Word wordValueQ0, const Word tagValueQ0, const Word tagValueQ1,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    S0cQ0wpQ1pValue s0cQ0wpQ1pValue(catS0, wordValueQ0, tagValueQ0, tagValueQ1, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, s0cQ0wpQ1pValue, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(s0cQ0wpQ1pValue);
    }
}


class S0cQ0pQ1wpAttributes::Impl: public AttributesImpl<S0cQ0pQ1wpValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<S0cQ0pQ1wpValue, MEDIUM, LARGE>("S0cQ0pQ1wpAttributes"){}
};

S0cQ0pQ1wpAttributes::S0cQ0pQ1wpAttributes(void): _impl(new Impl) {}
S0cQ0pQ1wpAttributes::S0cQ0pQ1wpAttributes(S0cQ0pQ1wpAttributes &other): _impl(share(other._impl)) {}
S0cQ0pQ1wpAttributes::~S0cQ0pQ1wpAttributes(void){ release(_impl); }

size_t S0cQ0pQ1wpAttributes::size(void) const { return _impl->size; }
void S0cQ0pQ1wpAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double S0cQ0pQ1wpAttributes::GetOrUpdateWeight(bool train, bool correct,
	const Cat *catS0, const Word tagValueQ0, const Word wordValueQ1, const Word tagValueQ1,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    S0cQ0pQ1wpValue s0cQ0pQ1wpValue(catS0, tagValueQ0, wordValueQ1, tagValueQ1, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, s0cQ0pQ1wpValue, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(s0cQ0pQ1wpValue);
    }
}


class S0cQ0pQ1pAttributes::Impl: public AttributesImpl<S0cQ0pQ1pValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<S0cQ0pQ1pValue, MEDIUM, LARGE>("S0cQ0pQ1pAttributes"){}
};

S0cQ0pQ1pAttributes::S0cQ0pQ1pAttributes(void): _impl(new Impl) {}
S0cQ0pQ1pAttributes::S0cQ0pQ1pAttributes(S0cQ0pQ1pAttributes &other): _impl(share(other._impl)) {}
S0cQ0pQ1pAttributes::~S0cQ0pQ1pAttributes(void){ release(_impl); }

size_t S0cQ0pQ1pAttributes::size(void) const { return _impl->size; }
void S0cQ0pQ1pAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double S0cQ0pQ1pAttributes::GetOrUpdateWeight(bool train, bool correct,
	const Cat *catS0, const Word tagValueQ0, const Word tagValueQ1,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    S0cQ0pQ1pValue s0cQ0pQ1pValue(catS0, tagValueQ0, tagValueQ1, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, s0cQ0pQ1pValue, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(s0cQ0pQ1pValue);
    }
}


class S0wcS1cS2cAttributes::Impl: public AttributesImpl<S0wcS1cS2cValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<S0wcS1cS2cValue, MEDIUM, LARGE>("S0wcS1cS2cAttributes"){}
};

S0wcS1cS2cAttributes::S0wcS1cS2cAttributes(void): _impl(new Impl) {}
S0wcS1cS2cAttributes::S0wcS1cS2cAttributes(S0wcS1cS2cAttributes &other): _impl(share(other._impl)) {}
S0wcS1cS2cAttributes::~S0wcS1cS2cAttributes(void){ release(_impl); }

size_t S0wcS1cS2cAttributes::size(void) const { return _impl->size; }
void S0wcS1cS2cAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double S0wcS1cS2cAttributes::GetOrUpdateWeight(bool train, bool correct,
	const Word wordValueS0, const Cat *catS0, const Cat *catS1, const Cat *catS2,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    S0wcS1cS2cValue s0wcS1cS2cValue(wordValueS0, catS0, catS1, catS2, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, s0wcS1cS2cValue, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(s0wcS1cS2cValue);
    }
}


class S0cS1wcS2cAttributes::Impl: public AttributesImpl<S0cS1wcS2cValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<S0cS1wcS2cValue, MEDIUM, LARGE>("S0cS1wcS2cAttributes"){}
};

S0cS1wcS2cAttributes::S0cS1wcS2cAttributes(void): _impl(new Impl) {}
S0cS1wcS2cAttributes::S0cS1wcS2cAttributes(S0cS1wcS2cAttributes &other): _impl(share(other._impl)) {}
S0cS1wcS2cAttributes::~S0cS1wcS2cAttributes(void){ release(_impl); }

size_t S0cS1wcS2cAttributes::size(void) const { return _impl->size; }
void S0cS1wcS2cAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double S0cS1wcS2cAttributes::GetOrUpdateWeight(bool train, bool correct,
	const Cat *catS0, const Word wordValueS1, const Cat *catS1, const Cat *catS2,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    S0cS1wcS2cValue s0cS1wcS2cValue(catS0, wordValueS1, catS1, catS2, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, s0cS1wcS2cValue, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(s0cS1wcS2cValue);
    }
}


class S0cS1cS2wcAttributes::Impl: public AttributesImpl<S0cS1cS2wcValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<S0cS1cS2wcValue, MEDIUM, LARGE>("S0cS1cS2wcAttributes"){}
};

S0cS1cS2wcAttributes::S0cS1cS2wcAttributes(void): _impl(new Impl) {}
S0cS1cS2wcAttributes::S0cS1cS2wcAttributes(S0cS1cS2wcAttributes &other): _impl(share(other._impl)) {}
S0cS1cS2wcAttributes::~S0cS1cS2wcAttributes(void){ release(_impl); }

size_t S0cS1cS2wcAttributes::size(void) const { return _impl->size; }
void S0cS1cS2wcAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double S0cS1cS2wcAttributes::GetOrUpdateWeight(bool train, bool correct,
	const Cat *catS0, const Cat *catS1, const Word wordValueS2, const Cat *catS2,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    S0cS1cS2wcValue s0cS1cS2wcValue(catS0, catS1, wordValueS2, catS2, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, s0cS1cS2wcValue, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(s0cS1cS2wcValue);
    }
}


class S0cS1cS2cAttributes::Impl: public AttributesImpl<S0cS1cS2cValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<S0cS1cS2cValue, MEDIUM, LARGE>("S0cS1cS2cAttributes"){}
};

S0cS1cS2cAttributes::S0cS1cS2cAttributes(void): _impl(new Impl) {}
S0cS1cS2cAttributes::S0cS1cS2cAttributes(S0cS1cS2cAttributes &other): _impl(share(other._impl)) {}
S0cS1cS2cAttributes::~S0cS1cS2cAttributes(void){ release(_impl); }

size_t S0cS1cS2cAttributes::size(void) const { return _impl->size; }
void S0cS1cS2cAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double S0cS1cS2cAttributes::GetOrUpdateWeight(bool train, bool correct,
	const Cat *catS0, const Cat *catS1, const Cat *catS2,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    S0cS1cS2cValue s0cS1cS2cValue(catS0, catS1, catS2, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, s0cS1cS2cValue, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(s0cS1cS2cValue);
    }
}


// 6


class S01cS01LcS01rcAttributes::Impl: public AttributesImpl<S01cS01LcS01rcValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<S01cS01LcS01rcValue, MEDIUM, LARGE>("S01cS01LcS01rcAttributes"){}
};

S01cS01LcS01rcAttributes::S01cS01LcS01rcAttributes(void): _impl(new Impl) {}
S01cS01LcS01rcAttributes::S01cS01LcS01rcAttributes(S01cS01LcS01rcAttributes &other): _impl(share(other._impl)) {}
S01cS01LcS01rcAttributes::~S01cS01LcS01rcAttributes(void){ release(_impl); }

size_t S01cS01LcS01rcAttributes::size(void) const { return _impl->size; }
void S01cS01LcS01rcAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double S01cS01LcS01rcAttributes::GetOrUpdateWeight(bool train, bool correct,
	const size_t type, Cat *cat0, Cat *cat1, Cat *cat2,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    S01cS01LcS01rcValue s01cS01LcS01rcValue(type, cat0, cat1, cat2, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, s01cS01LcS01rcValue, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(s01cS01LcS01rcValue);
    }
}


class S0cS0RcQ0pAttributes::Impl: public AttributesImpl<S0cS0RcQ0pValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<S0cS0RcQ0pValue, MEDIUM, LARGE>("S0cS0RcQ0pAttributes"){}
};

S0cS0RcQ0pAttributes::S0cS0RcQ0pAttributes(void): _impl(new Impl) {}
S0cS0RcQ0pAttributes::S0cS0RcQ0pAttributes(S0cS0RcQ0pAttributes &other): _impl(share(other._impl)) {}
S0cS0RcQ0pAttributes::~S0cS0RcQ0pAttributes(void){ release(_impl); }

size_t S0cS0RcQ0pAttributes::size(void) const { return _impl->size; }
void S0cS0RcQ0pAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double S0cS0RcQ0pAttributes::GetOrUpdateWeight(bool train, bool correct,
	const Cat *s0c, const Cat *s0rc, Word q0p,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    S0cS0RcQ0pValue s0cS0RcQ0pValue(s0c, s0rc, q0p, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, s0cS0RcQ0pValue, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(s0cS0RcQ0pValue);
    }
}


class S0cSLRcQ0S1wAttributes::Impl: public AttributesImpl<S0cS0LRcQ0S1w, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<S0cS0LRcQ0S1w, MEDIUM, LARGE>("S0cSLRcQ0S1wAttributes"){}
};

S0cSLRcQ0S1wAttributes::S0cSLRcQ0S1wAttributes(void): _impl(new Impl) {}
S0cSLRcQ0S1wAttributes::S0cSLRcQ0S1wAttributes(S0cSLRcQ0S1wAttributes &other): _impl(share(other._impl)) {}
S0cSLRcQ0S1wAttributes::~S0cSLRcQ0S1wAttributes(void){ release(_impl); }

size_t S0cSLRcQ0S1wAttributes::size(void) const { return _impl->size; }
void S0cSLRcQ0S1wAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double S0cSLRcQ0S1wAttributes::GetOrUpdateWeight(bool train, bool correct,
	const size_t type, const Cat *s0c, const Cat *cat, Word word,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    S0cS0LRcQ0S1w s0cS0LRcQ0S1w(type, s0c, cat, word, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, s0cS0LRcQ0S1w, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(s0cS0LRcQ0S1w);
    }
}


class S0cS0LS1cS1S1RcAttributes::Impl: public AttributesImpl<S0cS0LS1cS1S1RcValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<S0cS0LS1cS1S1RcValue, MEDIUM, LARGE>("S0cS0LS1cS1S1RcAttributes"){}
};

S0cS0LS1cS1S1RcAttributes::S0cS0LS1cS1S1RcAttributes(void): _impl(new Impl) {}
S0cS0LS1cS1S1RcAttributes::S0cS0LS1cS1S1RcAttributes(S0cS0LS1cS1S1RcAttributes &other): _impl(share(other._impl)) {}
S0cS0LS1cS1S1RcAttributes::~S0cS0LS1cS1S1RcAttributes(void){ release(_impl); }

size_t S0cS0LS1cS1S1RcAttributes::size(void) const { return _impl->size; }
void S0cS0LS1cS1S1RcAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double S0cS0LS1cS1S1RcAttributes::GetOrUpdateWeight(bool train, bool correct,
	const size_t type, Cat *cat0, Cat *cat1, Cat *cat2,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    S0cS0LS1cS1S1RcValue s0cS0LS1cS1S1RcValue(type, cat0, cat1, cat2, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, s0cS0LS1cS1S1RcValue, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(s0cS0LS1cS1S1RcValue);
    }
}


class S0wS1cS1RcAttributes::Impl: public AttributesImpl<S0wS1cS1RcValue, MEDIUM, LARGE>, public Shared {
public:
    Impl(void): AttributesImpl<S0wS1cS1RcValue, MEDIUM, LARGE>("S0wS1cS1RcAttributes"){}
};

S0wS1cS1RcAttributes::S0wS1cS1RcAttributes(void): _impl(new Impl) {}
S0wS1cS1RcAttributes::S0wS1cS1RcAttributes(S0wS1cS1RcAttributes &other): _impl(share(other._impl)) {}
S0wS1cS1RcAttributes::~S0wS1cS1RcAttributes(void){ release(_impl); }

size_t S0wS1cS1RcAttributes::size(void) const { return _impl->size; }
void S0wS1cS1RcAttributes::set_weights(const double *weights){
    _impl->set_weights(weights);
}

double S0wS1cS1RcAttributes::GetOrUpdateWeight(bool train, bool correct,
	const Word s0w, const Cat *s1c, const Cat *s1rc,
	ShiftReduceAction &action, double currentSent, double totalSent, double currentIter)
{
    S0wS1cS1RcValue s0wS1cS1RcValue(s0w, s1c, s1rc, action);
    if (train)
    {
	_impl->PerceptronUpdate(correct, s0wS1cS1RcValue, currentSent, totalSent, currentIter);
    }
    else
    {
	return _impl->find_weight(s0wS1cS1RcValue);
    }
}*/


} // namespace
} // namespace
