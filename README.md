This repository contains code for the paper:  
  
[Shift-Reduce CCG Parsing with a Dependency Model](http://www.aclweb.org/anthology/P14-1021)  
Wenduan Xu, Stephen Clark, and Yue Zhang  
In Proc. ACL 2014

Instructions to follow...